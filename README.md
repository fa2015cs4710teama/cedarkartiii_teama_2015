_CedarKart III_
===============
CedarKart III is a video game created by a team of Cedarville students for CS-4710 Computer Graphics. The CedarKart project has been a favorite for multiple years of the course, with students each year adding improvements and modifications to the game.  CedarKart III, and all previous versions of the game, were developed using [jMonkeyEngine](http://jmonkeyengine.org/).


# Architecture #
Here you'll find an overview of how CedarKart III is organized.

## Managers ##

The CedarKart work code is divided up amongst different Manager objects. Below you will find a brief description of the function of each class.

### GameManager ###

This manager is the main manager of the entire game. The Main class only references GameMan out of all of the managers. GameMan initializes all of the other managers and serves as a point person for a number of functions (e.g. calls to the network).

### GUIMangager ###

This manager interfaces with nifty GUI whose primary functionality is defined within files of the assets/Interface/Nifty/ directory. The interface here is responsible for triggering some initializations.

### HUDManager ###

This manager regulates the HUD (the display superimposed on the view). The HUDMan has an update function that is called every frame, but most of the work of the HUDMan should be requested by other managers. That is, other managers call functions of HUDMan when the gamestate changes (e.g. TrophyMan for minimap updates, PlayerMan for held item updates, ModeMan for objective updates).

### GameModeManager ###

This manager keeps up with the scoreboard and serves as a point person for the other mode managers (i.e. HuntModeMan, PaintModeMan, StockModeMan). Basically, GameModeMan knows which gameplay modes are enabled and picks a specific mode man to generate a new round after an objective is completed.

### ModeManager ###

This interface should be the only way that GameModeMan interacts with the specific mode men. When an objective is completed, GameModeMan picks a specific mode man and calls triggerNewRound() on it. That specific mode man will generate an objective and item placements. The objective will be sent to HUDMan (and the network) and the items will be sent to TrophyMan.

### TrophyManager ###

This manager keeps up with and maintains the items/trophies/powerups. Managers (e.g. a mode man, PlayerMan) must call TrophyMan to remove items from the world. TrophyMan holds the code for allowing PlayerMan to check if he can pick up an item. TrophyMan also sends messages to HUDMan (for minimap) (and the network).

### PlayerManager ###

This manager keeps up with the data and code concerning the player. PlayerMan's update function (called every frame) checks with TrophyMan if he can pick up an item. If he can, PlayerMan gets to decide what to do with the item and, if taking it, will send messages to TrophyMan (to remove), HUDMan (to show), and/or to GameModeMan (completed objective).

## Items ##

In the previous year's implementation, the goal of the game was to collect trophies (appearing one at a time and showing up on the minimap). Powerups were also located all over campus. Retrieving an item would randomly choose a powerup for the player from the following:

- BEES: block visibility
- TRAY: equip golf cart with single-shot tray
- IMPULSE: throw golf cart spinning into the air randomly
- GRAVITY: turn on zero gravity for the cart (not actually used)
- RANDOMIZER: randomly respawn cart on map

In the new gameplay (2015), an Item merely represents some generic physical thing, with properties, that can be passed around to different managers and across the network. Fundamentally, Items have a Placement and a type, but conceptually by extension of the type Items also (potentially) have information like a mesh file name, a minimap icon, a larger icon, and information about how the player interacts with the Item. Items thus represent powerups, objective items, and destinations (lumped here for reuse of collision code).

In multiplayer mode, certain powerups (BEES, IMPULSE, and RANDOMIZER) are applied to a player's competitors when collected, in order to annoy and deter them from reaching their goal.  Singleplayer mode also had powerups enabled, but with no competing players, all effects were applied to the player collecting the powerup.  Due to the annoyance this caused players, we disabled powerups in singleplayer mode.

## Networking ##

Networking was implemented as of 2013, and few changes warranting new documentation were made until late in the life of the 2015 fork.  That said, about half of the files within CedarKart's networking code have seen new commits, many of the changes cleaning up code that caused strange or inefficient behavior on the network. Most notably, a bug that caused the game to crash in multiplayer mode when the effect of a powerup expired was resolved.