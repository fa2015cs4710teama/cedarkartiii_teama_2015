package cedarkart;

import cedarkart.managers.GameManager;
import com.jme3.app.SimpleApplication;
import com.jme3.asset.AssetNotFoundException;
import com.jme3.system.AppSettings;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class Main extends SimpleApplication {
    // Constants
    public GameManager gameMan;

    public static void main(String[] args) {
        Main app = new Main();
        AppSettings settings = new AppSettings(true);
        settings.setUseJoysticks(true);
        
        // Set min resolution and default selected resolution
        settings.setMinResolution(1024, 768);
        settings.setResolution(1024, 768);
        
        // Branding

        settings.setTitle("CedarKart III");
        try{
            // Set window icon
            settings.setIcons(new BufferedImage[]{
                ImageIO.read(new File("assets/Interface/Settings/wheel_icon32.png")),
                ImageIO.read(new File("assets/Interface/Settings/wheel_icon64.png"))
            });
            // Set settings splash screen
            settings.setSettingsDialogImage("Interface/Settings/settingsSplash.png");
        }
        catch (IOException | AssetNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.WARNING, "Image file missing.", ex);
        }
        
        app.setSettings(settings);
        app.start();
    }

    @Override
    public void initialize() {
        super.initialize();
        setDisplayStatView(false);
    }

    @Override
    public void simpleInitApp() {
        this.gameMan = new GameManager(this, rootNode, guiNode,
                assetManager, inputManager, cam, flyCam, viewPort, stateManager,
                settings);
        gameMan.start();
    }

    @Override
    public void update() {
        super.update();
        gameMan.update();
    }

    @Override
    public void destroy() {
        gameMan.destroy();
        super.destroy();
    }

    public GameManager getGameMan() {
        return gameMan;
    }
}
