
package cedarkart.managers;

import cedarkart.Main;
import cedarkart.controllers.NiftyStartController;
import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Node;
import de.lessvoid.nifty.Nifty;

/**
 *
 * @author Paul Marshall
 */
public class GUIManager implements ICedarKartManager {
    
    private Main app;
    private BitmapFont font;
    private final Node guiNode;
    private SettingsManager settingsMan;
    
    private Nifty nifty;
    private NiftyJmeDisplay niftyDisplay;
    private NiftyStartController nsCtrl;
    
    public GUIManager(Main app, Node guiNode, AssetManager assetManager, SettingsManager settingsMan) {
        this.app = app;
        this.guiNode = guiNode;
        this.font = assetManager.loadFont("Interface/Fonts/Default.fnt");
        this.settingsMan = settingsMan;
    }

    @Override
    public void start() {
        // Main menu:
        this.nsCtrl = new NiftyStartController(this.app);
        
        this.niftyDisplay = new NiftyJmeDisplay(this.app.getAssetManager(),
                this.app.getInputManager(), this.app.getAudioRenderer(),
                this.app.getGuiViewPort());
        
        this.nifty = niftyDisplay.getNifty();
        this.nifty.fromXml("Interface/Nifty/xml/start.xml", "loading", this.nsCtrl);
        
        // Load additional XML (split from original start.xml for clarity)
        this.nifty.addXml("Interface/Nifty/xml/MP_mapSettings.xml");
        this.nifty.addXml("Interface/Nifty/xml/MP_networkSettings.xml");
        this.nifty.addXml("Interface/Nifty/xml/MP_vehicleSettings.xml");
        this.nifty.addXml("Interface/Nifty/xml/MP_modeSettings.xml");
        this.nifty.addXml("Interface/Nifty/xml/SP_mapSettings.xml");
        this.nifty.addXml("Interface/Nifty/xml/SP_vehicleSettings.xml");
        this.nifty.addXml("Interface/Nifty/xml/SP_modeSettings.xml");
        this.nifty.addXml("Interface/Nifty/xml/graphicsSettings.xml");
        this.nifty.addXml("Interface/Nifty/xml/controllerSettings.xml");
        this.nifty.addXml("Interface/Nifty/xml/gameStateScreens.xml");
    }
    
    public void start_overlay(boolean isHost) {
        if (isHost) {
            this.nsCtrl.load("start_mp");
        } else {
            this.nsCtrl.load("start_wait");
        }
    }
    
    public void show() {
        this.app.getGuiViewPort().addProcessor(this.niftyDisplay);
    }
    
    public void hide() {
        this.app.getGuiViewPort().removeProcessor(this.niftyDisplay);
    }
    
    public void main() {
        guiNode.detachAllChildren();
        this.nsCtrl.load("start");
    }
    
    public void loading() {
        this.nsCtrl.load("loading");
    }
    
    public void end(String endGameMessage) {
        //TODO: use endGameMessage
        System.out.println("GameOver with message: " + endGameMessage);
        
        this.nsCtrl.load("end");
        
        BitmapText currTrophy = new BitmapText(font);
        guiNode.attachChild(currTrophy);
        currTrophy.setColor(ColorRGBA.White);
        currTrophy.setSize(font.getPreferredSize() * 3f);
        currTrophy.setText(endGameMessage);
        currTrophy.setLocalTranslation(settingsMan.getWidth()/2 - currTrophy.getLineWidth()/2, settingsMan.getHeight()/2, 0);

    }
    
    public void pause(boolean me) {
        this.nsCtrl.pause(me);
    }
}
