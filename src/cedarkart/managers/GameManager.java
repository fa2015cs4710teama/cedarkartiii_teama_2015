
package cedarkart.managers;

import cedarkart.Main;
import cedarkart.managers.SettingsManager.Weather;
import cedarkart.managers.mode.GameModeManager;
import cedarkart.managers.mode.GameModeManager.GameType;
import cedarkart.network.ActorUpdate;
import cedarkart.network.ClientManager;
import cedarkart.network.ServerManager;
import cedarkart.managers.placement.PlacementManager;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bullet.control.VehicleControl;
import com.jme3.input.FlyByCamera;
import com.jme3.input.InputManager;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import java.util.ArrayList;

/**
 *
 * @author nabond
 */
public class GameManager implements ICedarKartManager {
    
    // Networking:
    private boolean isHost = false;
    private String host = null;
    private int port = 31337;

    private Main app;
    // Custom Managers
    private final ServerManager serverMan;
    private ClientManager clientMan;
    private final WorldManager worldMan;
    private final SettingsManager settingsMan;
    private final GameModeManager GameModeMan;
    private final PlacementManager placementMan;
    private final GUIManager GUIMan;
    private final HUDManager HUDMan;
    private final TrophyManager trophyMan;
    private final SoundManager soundMan;
    private boolean setupComplete = false;
    private boolean running = false;
    private boolean multiplayer = false;
    private boolean gameLoaded = false;
    private boolean gameInitialized = false;
    
    private Camera camera;
    
    public enum State {
        START,
        PAUSE,
        RESUME,
        END;
    }

    public GameManager(Main app, Node rootNode, Node guiNode,
            AssetManager assetManager, InputManager inputManager, Camera camera,
            FlyByCamera flyCam, ViewPort viewPort,
            AppStateManager stateManager, AppSettings settings) {
        this.app = app;

        this.serverMan = new ServerManager(app, this);
        this.settingsMan = new SettingsManager(settings);
        this.GUIMan = new GUIManager(this.app, guiNode, assetManager, this.settingsMan);
        this.GUIMan.start();

        this.HUDMan = new HUDManager(rootNode, guiNode, assetManager, camera,
                flyCam, this.settingsMan);
        
        this.soundMan = new SoundManager(rootNode, assetManager, settingsMan);
        
        this.worldMan = new WorldManager(rootNode, assetManager, inputManager,
                camera, flyCam, viewPort, stateManager, this.settingsMan, this, soundMan,
                this.HUDMan);
       
        this.trophyMan = new TrophyManager(assetManager, rootNode, this, worldMan,
                settingsMan, HUDMan, soundMan, GUIMan);
        
        this.placementMan = worldMan.getPlacementManager();
        
        this.GameModeMan = new GameModeManager(this);
        
        this.camera = camera;
    }

    /**
     * Start up the game with the loading screen
     */
    @Override
    public void start() {
        this.GUIMan.show();
    }
    
    /**
     * Load the game environment - this occurs after start() is called, 
     * so that a loading screen is shown before anything else happens.
     */
    public void loadGame(){
        this.soundMan.start();
        this.GameModeMan.init();
        this.resetWorld();
        this.gameLoaded = true;
    }
    
    
    public void destroy() {
        if (this.clientMan != null) {
            this.clientMan.disconnect();
        }
        if (this.serverMan != null) {
            this.serverMan.stop();
        }
    }
    
    public int getId() {
        if (this.clientMan != null) {
            return this.clientMan.getId();
        } else {
            return 0;
        }
    }
    
    public SettingsManager getSettingsMan() {
        return this.settingsMan;
    }
    
    public GameModeManager getGameModeMan() {
        return this.GameModeMan;
    }
    
    public ClientManager getClientMan() {
        return this.clientMan;
    }
    
    public ControllerManager getControlMan() {
        return this.worldMan.getControllerManager();
    }

    public HUDManager getHUDMan() {
        return this.HUDMan;
    }
    
    public PlacementManager getPlacementMan() {
        return this.worldMan.getPlacementManager();
    }
    
    public PlayerManager getPlayerMan() {
        return this.worldMan.getPlayerManager();
    }
    
    public TrophyManager getTrophyMan() {
        return this.trophyMan;
    }
    
    public WorldManager getWorldMan() {
        return this.worldMan;
    }
    
    public String getHost() {
        return this.host;
    }
    
    public void setHost(String host) {
        this.host = host;
    }
    
    public void host(boolean isHost) {
        this.isHost = isHost;
    }
    
    public boolean isHost() {
        return this.isHost;
    }
    
    public void startHost(GameType selectedMode, Weather map) {
        this.serverMan.start(selectedMode, map);
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
    
    public void connect() {
        this.GUIMan.show();

        if (this.isHost) {
            startHost(this.settingsMan.getGameMode(), this.settingsMan.getWeather());
            this.GUIMan.start_overlay(true);
        } else {
            this.GUIMan.start_overlay(false);
        }
        
        this.clientMan = new ClientManager(app, this, this.worldMan, this.worldMan.getPlayerManager());
    }
    
    public void disconnect() {
        this.clientMan.disconnect();
        this.clientMan = null;
    }
    
    public void sendItemsAdd(ArrayList<Item> itemList) {
        if (this.multiplayer) {
            this.clientMan.itemsAddSend(itemList);
        }        
    }

    public void sendItemRemove(Item item) {
        if (this.multiplayer) {
            this.clientMan.itemRemoveSend(item);
        }        
    }

    public void sendItemsClear() {
        if (this.multiplayer) {
            this.clientMan.itemsClearSend();
        }        
    }

    public void sendVehicleUpdate(VehicleControl vehicle) {
        if (this.multiplayer) {
            ActorUpdate update = new ActorUpdate(this.clientMan.getId(),
                    vehicle.getPhysicsLocation(), vehicle.getPhysicsRotationMatrix(),
                    vehicle.getLinearVelocity(), vehicle.getAngularVelocity());
            this.clientMan.update(update);
        }
    }
    
    public void sendCapture() {
        if (this.multiplayer) {
            this.clientMan.capture();
        }
    }
    
    public void sendMode(GameType mode) {
        if (this.multiplayer) {
            this.clientMan.modeSend(mode);
        }
    }
    
    public void sendObjective(String objective) {
        if (this.multiplayer) {
            this.clientMan.objectiveSend(objective);
        }
    }
    
    public void sendPowerup(Powerup powerup) {
        if (this.multiplayer) {
            this.clientMan.powerup(powerup);
        }
    }
    
    public void sendScore(int score, boolean isFinal) {
        if (this.multiplayer) {
            this.clientMan.scoreSend(score, isFinal);
        }
    }
    
    public void startGame() {
        
        // Different engines! This must be called before worldMan.start, which 
        //calls playerMan.start, which builds the vehicle, which attaches the 
        // engine sound to the vehicle and starts playing the sound
        soundMan.setEngineByVehicle(settingsMan.getVehicle());
        soundMan.setMusicByWeather(settingsMan.getWeather()); // MUSIC!
        
        this.camera.setLocation(new Vector3f(0.0f, 0.0f, 10.0f));
        this.camera.setRotation(new Quaternion(0.0f, 1.0f, 0.0f, 0.0f));
        this.worldMan.remakeWorld();
        this.GUIMan.hide();     
        this.worldMan.start();
        this.HUDMan.setPlayerMan(this.worldMan.getPlayerManager());
        this.HUDMan.start();
        this.trophyMan.start();
        
        if (!this.multiplayer) {
            setRunning(true);
            this.GameModeMan.triggerNewObjective();
        }
    }
    
    public void triggerGame() {
        if (this.multiplayer) {
            this.serverMan.setState(State.START);
            setRunning(true);
            this.GameModeMan.triggerNewObjective();
        }
    }
    
    public void pauseGame(boolean pausedByMe) {
        if (this.running) {     // filter out repeat events
            if (this.multiplayer) {
                this.clientMan.pause();
            }
            
            this.running = false;
            this.worldMan.getPhysicsManager().pause();
            this.GUIMan.pause(pausedByMe);
            this.GUIMan.show();
        }
    }
    
    public void resumeGame() {
        if (!this.running) {
            if (this.multiplayer) {
                this.clientMan.resume();
            }
            this.worldMan.getPhysicsManager().resume();
            this.running = true;
            this.GUIMan.hide();
        }
    }
    
    public void endGame(String endGameMessage) {
//        if (this.multiplayer && won()) {
//            this.clientMan.end();
//        }
        this.running = false;
        this.soundMan.stopEngineSound();
        this.HUDMan.removeHUD();
        this.GUIMan.end(endGameMessage);
        this.GUIMan.show();
    }
    
    public void quitGame() {
        this.running = false;
        this.HUDMan.removeHUD();
        this.soundMan.resetMusic();
        resetWorld();
    }
    
    public void resetWorld() {
        this.soundMan.stopEngineSound();
        this.worldMan.getControllerManager().reset();
        this.settingsMan.setWeather(SettingsManager.DEFAULT_WEATHER);
        this.settingsMan.setVehicle(SettingsManager.DEFAULT_VEHICLE);
        this.worldMan.remakeWorld();
        this.camera.setLocation(new Vector3f(-20.228601f, 10.419275f, 81.9956f));
        this.camera.setRotation(new Quaternion(0.0f, 0.5f, 0.0f, 0.7565786f));
        this.GUIMan.main();
    }

    public void update() {
        if(!this.gameLoaded){
            // Update loop must run once before anything can be rendered.
            // Having this flag here allows a static loading screen.
            if(!this.gameInitialized){
                this.gameInitialized = true;
            }
            else{
                this.loadGame();
            }
        }
        if (this.setupComplete) {
            PlayerManager playerMan = this.worldMan.getPlayerManager();
            this.HUDMan.update(this.running);
            if (this.running) {
                if (HUDMan.getTime() >= settingsMan.getGameTimeLength()) {
                    if(multiplayer) {
                        this.sendScore(playerMan.getPlayerScore(), true);
                    }
                    else {
                        this.endGame("You scored " + playerMan.getPlayerScore() + " points this game!");
                    }
                    trophyMan.reset();
                } else {
                    this.trophyMan.update();
                }
            }
            this.soundMan.updateEngineSound(playerMan.vehicle);
            this.worldMan.update();
        }
    }

    public void setSetupComplete(boolean isComplete) {
        this.setupComplete = isComplete;
        this.HUDMan.setStartTime();
    }
    
    public void setRunning(boolean running) {
        this.running = running;
        
        if (running) {
            this.GUIMan.hide();
            this.HUDMan.setStartTime();
            this.GameModeMan.init();
        }
    }
    
    public boolean isMultiplayer() {
        return multiplayer;
    }
    
    public void setMultiplayer(boolean multiplayer) {
        this.multiplayer = multiplayer;
    }
    
    //the following logic for winning by trophy gathering will eventually be 
    //removed and replaced by a timer expire and point checker logic
    public boolean won() {
//        return this.playerMan.getPlayerScore() == this.settingsMan.getWinningPaintCount();
        return true; //temporary code until we can determine winning player in multiplayer
    }
}
