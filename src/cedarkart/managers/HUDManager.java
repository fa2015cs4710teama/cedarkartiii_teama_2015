package cedarkart.managers;

import cedarkart.managers.Item.ItemType;
import cedarkart.managers.mode.GameModeManager.GameType;
import cedarkart.managers.placement.Placement;
import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.input.FlyByCamera;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class HUDManager implements ICedarKartManager {

    // Game Managers
    private PlayerManager playerMan;
    private final Node guiNode;
    private final Node rootNode;
    private final AssetManager assetManager;
    private SettingsManager settingsMan;
    private Camera camera;
    private FlyByCamera flyCam;
    private Picture miniMapPlayer;
    private Picture miniMapTrophy;
    private Picture trophyDisplay;
    private Picture powerUpDisplay;
    private AdvPicture miniMapImage;
    private Picture playerFirst;
    private Picture playerSecond;
    private Picture playerThird;
    private Picture playerFourth;
    private Picture playerMe;
    private Node miniMap;
    private Picture backPlayer;
    private Picture backMap;
    private Picture backObjective;
    private Picture backInfo;
    private Picture backLeader;
    private BitmapText hudText;
    private BitmapText playerTitle;
    private int terrainSize = 513;
    private Node markerNode;
    private Node trophyNode;
    //First 4 hold player id, second 4 hold scores
    private int[] scores = new int[4];
    private int[] players = new int[4];
    private BitmapText playerScore;
    private BitmapText topPlayer;
    private BitmapText objective;
    private BitmapText gameTimes;
    private BitmapFont font;
    private long start;
    private long diff;
    private final float mmReduce = 1.8f;
    private int time;
    private int scaleFactor = 6;

    static enum GUIMode {

        Settings, Race
    };

    HUDManager(Node rootNode, Node guiNode, AssetManager assetManager,
            Camera camera, FlyByCamera flyCam, SettingsManager settingsMan) {
        this.guiNode = guiNode;
        this.assetManager = assetManager;
        this.camera = camera;
        this.flyCam = flyCam;
        this.settingsMan = settingsMan;
        this.rootNode = rootNode;
        font = assetManager.loadFont("Interface/Fonts/Default.fnt");

    }

    @Override
    public void start() {
        initMiniMap();
        initBackgroundOverlay();
        initOverlay();
        //Win();
    }

    public void setPlayerMan(PlayerManager playerMan) {
        this.playerMan = playerMan;
    }

    public void initMiniMap() {
        //I choose to have the minimap all be on one node so that it is easy
        //to move the whole thing
        miniMap = new Node();
        //The guiNode is defaultly there and is where you can add things to the
        //hud
        guiNode.attachChild(miniMap);

        //setting up the map itself
        //I am just using the texturing pic as the minimap
        miniMapImage = new AdvPicture("Mini Map image", true);
        miniMapImage.setImage(assetManager, "Interface/Game/Minimap.png", true);
        miniMapImage.setWidth(terrainSize / mmReduce);
        miniMapImage.setHeight(terrainSize / mmReduce);
        miniMap.attachChild(miniMapImage);

        //setting maker for the player
        miniMapPlayer = new Picture("mini Map Marker");
        miniMapPlayer.setImage(assetManager, "Interface/Game/RedArrow.png", true);
        miniMapPlayer.setWidth(7);
        miniMapPlayer.setHeight(7);
        
        markerNode = new Node();
        markerNode.attachChild(miniMapPlayer);
        
        miniMap.attachChild(markerNode);
        //posistions are with the lower left hand corner being (0,0)
        miniMap.setLocalTranslation(-80f, 60, 0);
        
        trophyDisplay = new Picture("TrophyDisplay");
        trophyDisplay.setWidth(115);
        trophyDisplay.setHeight(115);
        trophyDisplay.setLocalTranslation(50, settingsMan.getHeight() - 155, 0);
        
        powerUpDisplay = new Picture("powerUpDisplay");
        powerUpDisplay.setWidth(115);
        powerUpDisplay.setHeight(115);
        powerUpDisplay.setLocalTranslation(50, settingsMan.getHeight() - 305, 0);
        
        objective = new BitmapText(font);
        objective.setColor(ColorRGBA.White);
        objective.setSize(font.getPreferredSize() * 1.25f);
    }

    public void initBackgroundOverlay() {
        backPlayer = new Picture("Overlay");
        backPlayer.setPosition(settingsMan.getWidth() - 180, settingsMan.getHeight() - 130);
        backPlayer.setWidth(180);
        backPlayer.setHeight(100);
        backPlayer.setImage(assetManager, "Interface/Game/overlay.png", true);
        backMap = new Picture("UnderMap");
        backMap.setLocalTranslation(-80f, 60, -1);
        backMap.setWidth((terrainSize / mmReduce) - 15);
        backMap.setHeight((terrainSize / mmReduce) + 15);
        backMap.setImage(assetManager, "Interface/Game/overlay.png", true);
        backObjective = new Picture("UnderObjective");
        backObjective.setLocalTranslation(-50,0,0);
        backObjective.setWidth(settingsMan.getWidth()+150);
        backObjective.setHeight(62);
        backObjective.setImage(assetManager, "Interface/Game/overlay.png", true);
        backLeader = new Picture("UnderLeader");
        backLeader.setLocalTranslation(settingsMan.getWidth() - 500, settingsMan.getHeight() - 130, 0);
        backLeader.setWidth(320);
        backLeader.setHeight(100);
        backLeader.setImage(assetManager, "Interface/Game/overlay.png", true);     
        backInfo = new Picture("UnderInfo");
        backInfo.setLocalTranslation(settingsMan.getWidth() - 180, settingsMan.getHeight() - 340, -1);
        backInfo.setWidth(180);
        backInfo.setHeight(200);
        backInfo.setImage(assetManager, "Interface/Game/overlay.png", true);
        
        playerTitle = new BitmapText(font);
        playerTitle.setColor(ColorRGBA.White);
        playerTitle.setText("You are player " + Integer.toString(playerMan.getPlayerId()));
        playerTitle.setLocalTranslation(settingsMan.getWidth() - 150, settingsMan.getHeight() - 150, 0);
        
        
        playerMe = new Picture("My ID");
        playerMe.setImage(assetManager, "Interface/Game/Player" + 
                Integer.toString(playerMan.getPlayerId()) + ".png", true);
        playerMe.setWidth(125);
        playerMe.setHeight(125);
        playerMe.setLocalTranslation(settingsMan.getWidth() - 150, settingsMan.getHeight() - 310, 0);
        guiNode.attachChild(playerMe);
        
        guiNode.attachChild(playerTitle);
        guiNode.attachChild(backInfo);
        guiNode.attachChild(backLeader);
        guiNode.attachChild(backObjective);
        guiNode.attachChild(backPlayer);
        guiNode.attachChild(backMap);
    }

    public void initOverlay() {
        initScores();
        gameTimes = new BitmapText(font);
        gameTimes.setColor(ColorRGBA.White);
        gameTimes.setText("Not Started");
        gameTimes.setLocalTranslation(settingsMan.getWidth() - 390, settingsMan.getHeight() - 50, 0);
        guiNode.attachChild(gameTimes);
    }

    public void update(boolean running) {
        //update minimap
        updatePlayerPosition();
        //update time
        if (running) {
            setTime();
        }
    }

    public void updateObjective(String newObjective) {
        String myText = newObjective;
        objective.setText(myText);
        objective.setLocalTranslation(settingsMan.getWidth()/2 - 4*myText.length(), 50, 0);
        guiNode.attachChild(objective);
    } 
    
    public void updatePowerupDisplay(Item powerup, int count) {
        ItemType myType = powerup.getType();
        
        powerUpDisplay.setImage(assetManager, "Interface/Game/Mascot.jpg", true);
        guiNode.attachChild(powerUpDisplay);
    }
    

    public void updateMap(GameType mode) {
        
    }
    
    public void clearTrophyDisplay(){
        trophyDisplay.removeFromParent();
    }
    
    public void updateTrophyDisplay(Item objectiveItem, int count) {
        ItemType myType = objectiveItem.getType();
        if (myType.equals(ItemType.PAINT_CAN)){       
            //Load different images instead of displaying text object
            switch (count){ 
                case 1:
                    trophyDisplay.setImage(assetManager, "Interface/Game/paintCan.png", true);
                    break;
                case 2:
                    trophyDisplay.setImage(assetManager, "Interface/Game/paintCanTimes2.png", true);
                    break;
                case 3:
                    trophyDisplay.setImage(assetManager, "Interface/Game/paintCanTimes3.png", true);
                    break;
            }
        }   
        else if (myType.equals(ItemType.PAPER)){        
            switch (count){
                case 1:
                    trophyDisplay.setImage(assetManager, "Interface/Game/paper.png", true);
                    break;
                case 2:
                    trophyDisplay.setImage(assetManager, "Interface/Game/paperTimes2.png", true);
                    break;
                case 3:
                    trophyDisplay.setImage(assetManager, "Interface/Game/paperTimes3.png", true);
                    break;
            }
        }
        guiNode.attachChild(trophyDisplay); 
    }
    
    private void updatePlayerPosition() {
        //translation
        //making the the player marker moves on the minimap correctly
        Vector3f vehiclePos = playerMan.getLocation();
        float x, y;
        x = ((vehiclePos.x + terrainSize) / (terrainSize)) * miniMapImage.width;
        y = ((-vehiclePos.z + terrainSize) / (terrainSize)) * miniMapImage.height;
        markerNode.setLocalTranslation(x - terrainSize / (mmReduce * 2), y - terrainSize / (mmReduce * 2) + 12, 1);

        //rotation
        //makes the marker point in the same direction of the camera
        Quaternion rotation = playerMan.getRotation();
        float[] angles = {0f, 0f, 0f};
        rotation.toAngles(angles);

        //atan2 has to be used to make it be used for full rotation
        //rotation.fromAngleAxis((float) (Math.atan2(rotation.getZ(), rotation.getX()) + Math.PI / 4), new Vector3f(0, 0, 1));
        rotation.fromAngleAxis(angles[1] - FastMath.PI, new Vector3f(0, 0, 1));
        markerNode.setLocalRotation(rotation);
    }

    public void updateItemPositions(ArrayList<Item> items) {
        //Cleanup
        miniMap.detachAllChildren();
        miniMap.attachChild(miniMapImage);
        miniMap.attachChild(markerNode);
        if (!items.isEmpty()){
            for(int i = 0; i < items.size(); i++){
                miniMapTrophy = new Picture("mini Map Marker");
                miniMapTrophy.setWidth(15);
                miniMapTrophy.setHeight(15);

                ItemType myType = (items.get(i)).getType(); //Assumes one type in array
                if (myType.equals(ItemType.DESTINATION)){
                    miniMapTrophy.setImage(assetManager, "Interface/Game/gold-trophy-icon.png", true);
                }
                else if (myType.equals(ItemType.MASCOT)){
                    miniMapTrophy.setImage(assetManager, "Interface/Game/Mascot.png", true);
                }
                else if (myType.equals(ItemType.PAINT_CAN)){
                    miniMapTrophy.setImage(assetManager, "Interface/Game/paintCan.png", true);
                }
                else if (myType.equals(ItemType.PAPER)){
                    miniMapTrophy.setImage(assetManager, "Interface/Game/paper.png", true);
            } 
                Placement place = (items.get(i)).getPlacement();
                Vector3f placeLoc = place.getLocation();
                float x = placeLoc.x;
                float z = placeLoc.z;
                trophyNode = new Node();
                trophyNode.attachChild(miniMapTrophy);
                miniMap.attachChild(trophyNode);
                updateTrophyPosition(x, z);
            }
        }
    }
    
    public void updateTrophyPosition(float X, float Z) {
        float x, y;
        x = ((X + terrainSize) / (terrainSize)) * miniMapImage.width;
        y = ((-Z + terrainSize) / (terrainSize)) * miniMapImage.height;
        trophyNode.setLocalTranslation(x - scaleFactor - terrainSize / 
                (mmReduce * 2), y - scaleFactor - terrainSize / 
                (mmReduce * 2) + 12, 0);
    }

    public void updateScore(Integer playerID, Integer score) {       
        int myPlace = 0;
        for(int i = 0; i < 4; i++){
            if(players[i] == playerID){
                scores[i] = score;
                myPlace = i;
                break;
            }
        }
        //Sort scores
        if (score != 0){
            sortScores(myPlace);
        }    
        redrawScore();
    }
    
    public void initScores(){
        playerScore = new BitmapText(font);
        playerScore.setColor(ColorRGBA.White);
        playerScore.setSize(font.getPreferredSize() * 1.0f);
        //Display leader info
        topPlayer = new BitmapText(font);
        topPlayer.setColor(ColorRGBA.White);
        topPlayer.setSize(font.getPreferredSize() * 1.25f);
        topPlayer.setText("No players have scored");
        //Player Icons
        playerFirst = new Picture("First Player");
        playerFirst.setImage(assetManager, "Interface/Game/Player1.png", true);
        playerFirst.setWidth(20);
        playerFirst.setHeight(20);
        playerFirst.setLocalTranslation(settingsMan.getWidth() - 166, settingsMan.getHeight() - 60, 0);
        playerSecond = new Picture("Second Player");
        playerSecond.setImage(assetManager, "Interface/Game/Player2.png", true);
        playerSecond.setWidth(20);
        playerSecond.setHeight(20);
        playerSecond.setLocalTranslation(settingsMan.getWidth() - 166, settingsMan.getHeight() - 80, 0);
        playerThird = new Picture("Third Player");
        playerThird.setImage(assetManager, "Interface/Game/Player3.png", true);
        playerThird.setWidth(20);
        playerThird.setHeight(20);
        playerThird.setLocalTranslation(settingsMan.getWidth() - 166, settingsMan.getHeight() - 100, 0);
        playerFourth = new Picture("Third Player");
        playerFourth.setImage(assetManager, "Interface/Game/Player4.png", true);
        playerFourth.setWidth(20);
        playerFourth.setHeight(20);
        playerFourth.setLocalTranslation(settingsMan.getWidth() - 166, settingsMan.getHeight() - 120, 0);
        //Where to display text
        playerScore.setLocalTranslation(settingsMan.getWidth() - 140, settingsMan.getHeight() - 40, 0);
        topPlayer.setLocalTranslation(settingsMan.getWidth() - 445, settingsMan.getHeight() - 70, 0);
        guiNode.attachChild(playerScore);
        guiNode.attachChild(topPlayer);
        guiNode.attachChild(playerFirst);
        guiNode.attachChild(playerSecond);
        guiNode.attachChild(playerThird);
        guiNode.attachChild(playerFourth);
        //init array
        for(int i = 0; i < 4; i++){
            players[i] = i + 1;
            scores[i] = 0;
        }
        redrawScore();
    }
    
    public void redrawScore(){   
        //Set text
        playerScore.setText("Player " + players[0] + " :      " + scores[0] + '\n' + 
                            "Player " + players[1] + " :      " + scores[1] + '\n' +
                            "Player " + players[2] + " :      " + scores[2] + '\n' +
                            "Player " + players[3] + " :      " + scores[3] + '\n' );   
    }
    
    private String whichIcon(int Place){
        switch (players[Place]){
            case 1:
                return "Interface/Game/Player1.png";
            case 2:
                return "Interface/Game/Player2.png";
            case 3:
                return "Interface/Game/Player3.png";
            case 4:
                return "Interface/Game/Player4.png";
        }
        return "Wrong Reference";
                
    }
    
    public void sortScores(int myPlace){
        for(int j = myPlace; j >= 0; j--){
            //First to make point stays on top
            if(scores[myPlace]>scores[j]){ 
                //Swap player and corresponding score
               int tempScore = scores[j];
               int tempPlayer = players[j];
               scores[j] = scores[myPlace];
               players[j] = players[myPlace];
               scores[myPlace] = tempScore;
               players[myPlace] = tempPlayer;       
            }
        }
        playerFirst.setImage(assetManager, whichIcon(0), true);
        playerSecond.setImage(assetManager, whichIcon(1), true);
        playerThird.setImage(assetManager, whichIcon(2), true);
        playerFourth.setImage(assetManager, whichIcon(3), true);
        topPlayer.setText("Player " + players[0] + " is in the lead!");
    }

    public void setStartTime() {
        this.start = System.currentTimeMillis();
    }

    public void setTime() {
        long end = System.currentTimeMillis();
        diff = (end - start) / 1000;
        time = (int)diff;
        DecimalFormat nf2 = new DecimalFormat("#00");
        gameTimes.setText("Time: " + (int) (diff / 60) + ":" + nf2.format(diff % 60));
    }
    public int getTime(){
        return time;
    }
    
    public void removeHUD() {
        markerNode.removeFromParent();
        trophyNode.removeFromParent();
        miniMap.removeFromParent();
        backPlayer.removeFromParent();
        backMap.removeFromParent();
        backObjective.removeFromParent();
        backLeader.removeFromParent();
        backInfo.removeFromParent();
        playerFirst.removeFromParent();
        playerSecond.removeFromParent();
        playerThird.removeFromParent();
        playerFourth.removeFromParent();
        playerMe.removeFromParent();
        playerTitle.removeFromParent();
        topPlayer.removeFromParent();
        playerScore.removeFromParent();
        gameTimes.removeFromParent();
        powerUpDisplay.removeFromParent();
        trophyDisplay.removeFromParent();
        objective.removeFromParent();
    }
}
