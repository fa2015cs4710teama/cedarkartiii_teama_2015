package cedarkart.managers;

import cedarkart.managers.placement.Placement;
import com.jme3.network.serializing.Serializable;
import java.lang.annotation.Annotation;

/**
 * This class is intended to allow the passing of paint cans, destinations, etc.
 * 
 * I think this will need to be defined as we discover need. One of the other existing
 * data structures may work (e.g. Placement, Powerup)
 * 
 * @author Grant Dennison
 */
@Serializable
public class Item implements Serializable {
    
    public enum ItemType {
        PAINT_CAN,
        PAPER,
        MASCOT,
        DESTINATION,
    }
    
    private Placement place;
    private ItemType type;
    
    public Item() {
        
    }
    
    public Item(Placement place, ItemType type) {
        this.place = place;
        this.type = type;
    }
    
    @Override
    public boolean equals(Object other) {
        if(!(other instanceof Item)) {
            return false;
        }
        
        Item otherItem = (Item) other;
        return place.equals(otherItem.getPlacement());
    }
    
    public String getIconFileName() {
        return "";
    }
    
    public String getMapIconFileName() {
        return "";
    }

    public String getMeshFileName() {
        return "";
    }
    
    public Placement getPlacement() {
        return this.place;
    }
    
    public ItemType getType() {
        return this.type;
    }

    @Override
    public Class serializer() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public short id() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
