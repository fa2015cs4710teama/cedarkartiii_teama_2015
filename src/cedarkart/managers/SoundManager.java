package cedarkart.managers;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.control.VehicleControl;
import com.jme3.scene.Node;
import cedarkart.managers.SettingsManager.Weather;
import cedarkart.managers.VehicleManager.Vehicle;

public class SoundManager implements ICedarKartManager {
    
    // Audio nodes - audioEngine and backgroundMusic in particular must be 
    // class variables so that they can be detached and re-attached from the
    // root node easily
    private AudioNode audioEngine;
    protected AudioNode trophyCollectedSound;
    protected AudioNode powerupCollectedSound;
    protected AudioNode backgroundMusic;
    
    // Standard Managers
    private AssetManager assetManager;
    
    // Custom Managers
    private SettingsManager settingsMan;

    private Node rootNode;
    private boolean isAccelerating;
    
    // Settings variables - will likely be stored in SettingsManager in future
    private float volumeMaster;
    private float volumeMusic;
    private float volumeSFX;
    
    // AudioNode.setPitch only accepts values between 0.5f and 2.0f - 
    // we can use these to set min and max any time we do pitch shifting
    private final float MIN_PITCH = 0.5f;
    private final float MAX_PITCH = 2.0f;
    
    // Engine sound modifiers. Engine audio volumes will 
    // be multiplied by VOLUME_SFX in future
    private int ENGINE_HP;
    private final float ENGINE_MIN_VOLUME = 3.0f;
    private final float ENGINE_MAX_VOLUME = 4.0f;
    
    // Sound files
    private final String MUSIC_DEFAULT =    "Sounds/Music/News_Theme_loop.wav";
    private final String MUSIC_SUNRISE =    "Sounds/Music/Riptide.ogg";
    private final String MUSIC_NIGHT =      "Sounds/Music/Killing_Time.wav";
    private final String MUSIC_APOCOLYPSE = "Sounds/Music/Phantom_from_Space.ogg";
    private final String MUSIC_WINTER = "Sounds/Music/Joy_to_the_World.wav";
    
    private final String SFX_ENGINE_GOLF_CART = "Sounds/Engine/smallEngine.wav";
    private final String SFX_ENGINE_DEFAULT = "Sounds/Engine/largeEngine.wav";
    private final String SFX_TROPHY_COLLECT = "Sounds/CollectionSounds/TrophyCollectedSound.wav";
    private final String SFX_POWERUP_COLLECT = "Sounds/CollectionSounds/PowerupCollectedSound.wav";
    
    public SoundManager(Node rootNode, AssetManager assetManager, 
            SettingsManager settingsMan) {
        this.volumeMaster = 1.0f;
        this.volumeMusic = 0.3f;
        this.volumeSFX = 1.0f;
        
        this.rootNode = rootNode;
        this.assetManager = assetManager;
        this.settingsMan = settingsMan;
    }
    
    @Override
    public void start() {
        if(audioEngine == null){
            this.resetEngine();
        }
        
        if(backgroundMusic == null){
            this.resetMusic();
        }
        
        trophyCollectedSound = createSFXAudioNode(this.SFX_TROPHY_COLLECT);
        rootNode.attachChild(trophyCollectedSound);
        
        powerupCollectedSound = createSFXAudioNode(this.SFX_POWERUP_COLLECT);
        rootNode.attachChild(powerupCollectedSound);
    }
    
    /**
     * Create an AudioNode that is non-positional, non-looping, with our set 
     * SFX volume.
     * 
     * @param filename
     * @return 
     */
    private AudioNode createSFXAudioNode(String filename){
        AudioNode effect = new AudioNode(assetManager, filename, false);
        effect.setPositional(false);
        effect.setLooping(false);
        effect.setVolume(this.volumeSFX);
        return effect;
    }

    // Engine-sound-related functions

    public void setAccelerating(boolean isPressed) {
        isAccelerating = isPressed;
    }
    
    /**
     * Function for attaching vehicle sound to a specific vehicleNode, used in 
     * WorldManager, PlayerManager. Currently causes other players' engines to 
     * sound exactly like local player's engine, including pitch-shifting on 
     * local player's acceleration.
     * 
     * @param vehicleNode 
     */
    public void attachVehicleSound(Node vehicleNode){
        vehicleNode.attachChild(audioEngine);
        audioEngine.play();
    }
    
    public void stopEngineSound(){
        audioEngine.stop();
    }

    /**
     * Update the sound of the engine based on whether the accelerator is 
     * being pressed, and the vehicle's current speed.
     * @param vehicle 
     */
    public void updateEngineSound(VehicleControl vehicle) {
        // Get absolute value of current vehicle speed
        float engineSpeed = vehicle.getCurrentVehicleSpeedKmHour();
        engineSpeed = (engineSpeed <= 0.0F) ? 0.0F - engineSpeed : engineSpeed;
        
        // Set pitch engine pitch based on speed
        float newPitch = engineSpeed / this.ENGINE_HP;
        if(isAccelerating){
            // Add a few RPMs while accelerating
            newPitch += (0.0005f * this.ENGINE_HP);
            pitchShiftEngine(newPitch);
            audioEngine.setVolume(this.ENGINE_MAX_VOLUME);
        }
        else{
            pitchShiftEngine(newPitch);
            audioEngine.setVolume(this.ENGINE_MIN_VOLUME);
        }
    }
    
    /**
     * Set pitch within min and max bounds, based on incoming
     * @param newPitch 
     */
    private void pitchShiftEngine(float newPitch){
        // Pitch should never be less than MIN_PITCH
        newPitch += this.MIN_PITCH;
        if(newPitch >= this.MAX_PITCH){
            audioEngine.setPitch(this.MAX_PITCH);
        }
        else{
            audioEngine.setPitch(newPitch);
        }
    }

    public void playTrophySound(){
        trophyCollectedSound.play();
    }
    
    public void playPowerupSound(){
        powerupCollectedSound.play();
    }
    
    /**
     * Set engine sound by vehicle.
     * @param vehicle 
     */
    public void setEngineByVehicle(Vehicle vehicle){
        switch(vehicle){
            case GOLF_CART:
                // Golf cart is OP! How else would it go over 15mph?
                this.ENGINE_HP = 100;
                this.setEngine(this.SFX_ENGINE_GOLF_CART);
                break;
            case ATOM_2:
                this.ENGINE_HP = 245;
                this.setEngine(this.SFX_ENGINE_GOLF_CART);
                break;
            case VIPER:
                this.ENGINE_HP = 525;
                this.setEngine(this.SFX_ENGINE_DEFAULT);
                break;
            default:
                this.ENGINE_HP = 100;
                this.setEngine(this.SFX_ENGINE_DEFAULT);
        }
    }
    
    /**
     * Set engine sound by filename.
     * @param filename 
     */
    private void setEngine(String filename){
        // If backgroundMusic is already attached, stop and detach, saving index
        // to let new music take its old spot
        int attachIndex = -1;
        if(audioEngine != null){
            audioEngine.stop();
            attachIndex = rootNode.detachChild(audioEngine);
        }
        
        // Set up new audioEngine
        audioEngine = new AudioNode(assetManager, filename, false);
        audioEngine.setPositional(true);
        audioEngine.setLooping(true);
        audioEngine.setPitch(this.MIN_PITCH);
        audioEngine.setVolume(this.ENGINE_MIN_VOLUME);
        //vehicleNode.attachChild(audioEngine);
        //audioEngine.play();
        
        // Attach varies depending on if music was previously attached
        if(attachIndex != -1){
            rootNode.attachChildAt(audioEngine, attachIndex);
        }
        else{
            rootNode.attachChild(audioEngine);
        }
    }
    
    /**
     * Reset engine sound to default.
     */
    public void resetEngine(){
        this.setEngineByVehicle(settingsMan.DEFAULT_VEHICLE);
    }
    
    /**
     * Set different music based on loaded map (Weather)
     * @param weather 
     */
    public void setMusicByWeather(Weather weather){
        
        switch(weather){
            case SUNRISE:
                this.setMusic(MUSIC_SUNRISE, volumeMusic);
                break;
            case NIGHT:
                this.setMusic(MUSIC_NIGHT, volumeMusic);
                break;
            case APOCOLYPSE:
                this.setMusic(MUSIC_APOCOLYPSE, volumeMusic);
                break;
            case WINTER:
                this.setMusic(MUSIC_WINTER, volumeMusic);
                break;
            default:
                this.resetMusic();
                break;
        }
    }
    
    /**
     * Set music by filename
     * @param filename 
     */
    public void setMusic(String filename, float volume){
        
        // If backgroundMusic is already attached, stop and detach, saving index
        // to let new music take its old spot
        int attachIndex = -1;
        if(backgroundMusic != null){
            backgroundMusic.stop();
            attachIndex = rootNode.detachChild(backgroundMusic);
        }
        
        // Set up new backgroundMusic
        backgroundMusic = new AudioNode(assetManager, filename, false);
        backgroundMusic.setPositional(false);
        backgroundMusic.setLooping(true);
        backgroundMusic.setVolume( volume );
        
        // Attach varies depending on if music was previously attached
        if(attachIndex != -1){
            rootNode.attachChildAt(backgroundMusic, attachIndex);
        }
        else{
            rootNode.attachChild(backgroundMusic);
        }
        backgroundMusic.play();
    }
    
    /**
     * Resets game music to SoundManager.MUSIC_DEFAULT.
     */
    public void resetMusic(){
        setMusic(MUSIC_DEFAULT, volumeMusic);
    }
}
