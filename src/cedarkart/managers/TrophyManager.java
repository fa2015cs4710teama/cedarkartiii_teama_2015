package cedarkart.managers;

import cedarkart.managers.placement.PlacementManager;
import cedarkart.managers.placement.Placement;
import cedarkart.Counter;
import cedarkart.managers.Powerup.Type;
import cedarkart.managers.Item.ItemType;
import com.jme3.asset.AssetManager;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author Gregory
 */
public class TrophyManager implements ICedarKartManager {

    private final AssetManager assetManager;
    private final PlayerManager playerMan;
    private final SettingsManager settingsMan;
    private final Node rootNode;
    private final GameManager gameMan;
    private final WorldManager worldMan;
    private final HUDManager HUDMan;
    private final PlacementManager placementMan;
    private final SoundManager soundMan;
    private final GUIManager GUIMan;
    private final long POWERUP_RESPAWN_TIME = 17000;
    
    private ArrayList<Item> items = new ArrayList<>();
    
    private ArrayList<Placement> placements = new ArrayList<>();
    private ArrayList<Placement> placement = new ArrayList<>();
    private ArrayList<Placement> powerPlacements = new ArrayList<>();
    private ArrayList<Placement> itemPlacements = new ArrayList<>();
    private ArrayList<Placement> rotatingItemPlacements = new ArrayList<>();
    private ArrayList<Placement> usedPowerPlacements = new ArrayList<>();
    private ArrayList<Long> powerResetStartTime = new ArrayList<>();
    private final Counter count = new Counter();
    private Random generator = new Random();

    public TrophyManager(AssetManager assetManager, Node rootNode, GameManager gameMan,
            WorldManager worldMan, SettingsManager settingsMan, HUDManager HUDMan,
            SoundManager soundMan, GUIManager GUIMan) {
        this.assetManager = assetManager;
        this.rootNode = rootNode;
        this.gameMan = gameMan;
        this.worldMan = worldMan;
        this.settingsMan = settingsMan;
        this.HUDMan = HUDMan;
        this.placementMan = worldMan.getPlacementManager();
        this.playerMan = worldMan.getPlayerManager();
        this.soundMan = soundMan;
        this.GUIMan = GUIMan;
    }

    @Override
    public void start() {
        if(this.gameMan.isMultiplayer()) {
            powerPlacements = placementMan.createPowerUpPlacements();
            for(int i = 0; i < powerPlacements.size(); i++) {
                powerResetStartTime.add((long) -1);
            }
            placementMan.addTrophyPlacements(powerPlacements, false);
        }
    }
    
    public void gameStart() {
        placeTrophy();
    }

    
    public void update() {
        
        //items
        placementMan.rotateObjects(rotatingItemPlacements);
        placementMan.bounceObjects(rotatingItemPlacements, (float)count.next());

        float playerX = playerMan.getLocation().x;
        float playerY = playerMan.getLocation().y;
        float playerZ = playerMan.getLocation().z;

        //powerups
        float powerDist;
        float powerX;
        float powerY;
        float powerZ;
        Placement currPlacement;
        for (int i = 0; i < powerPlacements.size(); i++) {
            boolean isPowerupOut = powerResetStartTime.get(i) < 0;
            if(!isPowerupOut) {
                continue;
            }
            
            currPlacement = powerPlacements.get(i);
            powerX = currPlacement.getLocation().x;
            powerY = currPlacement.getLocation().y;
            powerZ = currPlacement.getLocation().z;
            powerDist = (float) (FastMath.sqr(playerX - powerX) + FastMath.sqr(playerY + 0.5f - powerY) + FastMath.sqr(playerZ - powerZ));
            //if you get close enough to the powerup box...
            if (powerDist < 1f) {
                soundMan.playPowerupSound();
                capturePowerup(i);
                if (!this.gameMan.isMultiplayer()) {
//                    int type = FastMath.nextRandomInt(1, 4);
//                    switch(type) {
//                        case 1:
//                            playerMan.setBees(true);
//                            break;
//                        case 2:
//                            playerMan.enableTray();
//                            break;
//                        case 3:
//                            playerMan.impulsePowerup();
//                            break;
//                        case 4:
//                            playerMan.randomizeLocation();
//                            break;
//                        case 5:
//                            playerMan.setGravity(false);
//                            break;
//                    }
                } else {
                    int type = FastMath.nextRandomInt(1, 3);
                    Type pType = Type.values()[type - 1];
                    this.gameMan.sendPowerup(new Powerup(i, -1, pType));
                }
            }
        }

        long currTime = System.currentTimeMillis();
        for (int i = 0; i < powerPlacements.size(); i++) {
            long resetTime = powerResetStartTime.get(i);
            if (resetTime > 0 && currTime - resetTime >= POWERUP_RESPAWN_TIME) {
                currPlacement = powerPlacements.get(i);
                ArrayList<Placement> addPlacement = new ArrayList<>();
                addPlacement.add(currPlacement);
                powerResetStartTime.set(i, (long) -1);
                placementMan.addTrophyPlacements(addPlacement, false);

                playerMan.setBees(false);
//                playerMan.setGravity(true);
            }
        }
    }

    public void reset(){
        
            playerMan.resetPlayerScore();
            placementMan.removePlacements(placement);
        
            Placement currPlacement;
        for (int i = 0; i < usedPowerPlacements.size(); i++) {
            currPlacement = powerPlacements.get(i);
            ArrayList<Placement> addPlacement = new ArrayList<>();
            addPlacement.add(currPlacement);
            usedPowerPlacements.remove(currPlacement);
            powerResetStartTime.remove(i);
            placementMan.addTrophyPlacements(addPlacement,false);
            powerPlacements.add(currPlacement);
        }
    }
    
    public void setSeed(int seed) {
        this.generator.setSeed(seed);
    }
    
    public void capturePowerup(int id) {
        if (id >= powerPlacements.size()) {
            System.err.println("[POWERUP] id is out of range.  Powerup may not be properly removed.");
            return;
        }
        
        Placement currPlacement = powerPlacements.get(id);
        ArrayList<Placement> removePlacement = new ArrayList<>();
        removePlacement.add(currPlacement);
        powerResetStartTime.set(id, System.currentTimeMillis());
        placementMan.removePlacements(removePlacement);
    }
    
    /** 
     * clears all items from the game map, called at the beginning of 
     * each round 
     */
    public void clearItems() {
        placementMan.removePlacements(itemPlacements);
        HUDMan.clearTrophyDisplay();
        this.items.clear();
        this.itemPlacements.clear();
        this.rotatingItemPlacements.clear();
        
        HUDMan.updateItemPositions(this.items);
    }
    public void clearItemsAndBroadcast() {
        gameMan.sendItemsClear();
        clearItems();
    }
    
    /** 
     * places all new items on the game map, called at the 
     * beginning of each round 
     * @param newItems
     */
    public void placeItems(ArrayList<Item> newItems) {
        this.items.addAll(newItems);
                
        for(int i = 0; i < newItems.size(); i++) {
            itemPlacements.add(newItems.get(i).getPlacement());
            if(newItems.get(i).getType()==ItemType.PAINT_CAN){
                rotatingItemPlacements.add(newItems.get(i).getPlacement());
            }
            else if(newItems.get(i).getType()==ItemType.MASCOT){
                rotatingItemPlacements.add(newItems.get(i).getPlacement());
            }
        }
        
        placementMan.addPlacements(itemPlacements, false);
        HUDMan.updateItemPositions(this.items);
    }
    public void placeItemsAndBroadcast(ArrayList<Item> newItems) {
        gameMan.sendItemsAdd(newItems);
        placeItems(newItems);
    }

    private int placeTrophy() {

        int temp = generator.nextInt(placements.size());
        placement.add(placements.remove(temp));
        placementMan.addTrophyPlacements(placement, false);
        
        Vector3f trophyLocation = placement.get(0).getLocation();
        HUDMan.updateTrophyPosition(trophyLocation.x, trophyLocation.z);
        return temp;
    }
    
    /**  Determines if the player is close enough to pick up an item;
     * if an item is in range, return it to Playerman 
     */
    public Item playerCollectItem() {
        Vector3f playerLocation = playerMan.getLocation();
        float playerX = playerLocation.x;
        float playerY = playerLocation.y;
        float playerZ = playerLocation.z;
        
        for (int i = 0; i < this.items.size(); i++) {
            Item currItem = this.items.get(i);
            Placement currPlacement = currItem.getPlacement();
            
            Vector3f currLocation = currPlacement.getLocation();
            float itemX = currLocation.x;
            float itemY = currLocation.y;
            float itemZ = currLocation.z;
            float itemDist = (float) (FastMath.sqr(playerX - itemX) + FastMath.sqr(playerY + 0.5f - itemY) + FastMath.sqr(playerZ - itemZ));
            //if you get close enough to the item...
            if ((currItem.getType() == ItemType.PAINT_CAN ||
                    currItem.getType() == ItemType.MASCOT) && itemDist < 5f) {
                return currItem;
            } 
            else if ((currItem.getType() == ItemType.DESTINATION ||
                    currItem.getType() == ItemType.PAPER) && itemDist < 350f) {
                return currItem;       
            }
        }    
        
        return null;
    }
   
    /** 
     * called by PlayerMan to remove a collected item 
     * @param item
     */
    public void removeItem(Item item) {

        placementMan.removePlacement(item.getPlacement());
        itemPlacements.remove(item.getPlacement());
        rotatingItemPlacements.remove(item.getPlacement());
 
        items.remove(item);
        HUDMan.updateItemPositions(this.items);
    }
    public void removeItemAndBroadcast(Item item) {
        gameMan.sendItemRemove(item);
        
        removeItem(item);
    }
}
