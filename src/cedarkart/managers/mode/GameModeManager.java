package cedarkart.managers.mode;

import cedarkart.managers.GameManager;
import cedarkart.managers.HUDManager;
import cedarkart.managers.PlayerManager;
import cedarkart.managers.SettingsManager;
import cedarkart.managers.TrophyManager;
import java.util.Random;

public class GameModeManager {
    private final GameManager gameMan;
    private HuntModeManager huntMan;
    private PaintModeManager paintMan;
    private StockModeManager stockMan;
    private TrophyManager trophyMan;
    private PlayerManager playerMan;
    private HUDManager HUDMan;
    
    private GameType selectedMode;
    private GameType activeMode;
    
    public enum GameType {
        HUNT,
        STOCK,
        PAINT,
        PARTY
    }
    
    public GameModeManager(GameManager gameMan) {
        this.gameMan = gameMan;
        this.trophyMan = gameMan.getTrophyMan();
        this.HUDMan = gameMan.getHUDMan();
        this.playerMan = gameMan.getPlayerMan();
    }
    
    public void init() {
        this.stockMan = new StockModeManager(this.gameMan);
        this.paintMan = new PaintModeManager(this.gameMan);
        this.huntMan = new HuntModeManager(this.gameMan);
        selectedMode = gameMan.getSettingsMan().getGameMode();
    }
    
    /** 
     * Called by network to update players on new round 
     */
    public void feedNewObjective() {

    }
    
    /** 
     * Called by Playerman to begin a new round 
     */
    public void triggerNewObjective() {

        if(selectedMode == GameType.PARTY) {
            activeMode = this.randomModeGenerator();
        } else {
            activeMode = selectedMode;
        }

        gameMan.sendMode(activeMode);
        trophyMan.clearItemsAndBroadcast(); //clear map of items       
        playerMan.resetCollections(); // empty all player items and destinations
        HUDMan.updateScore(playerMan.getPlayerId(), playerMan.getPlayerScore());
        HUDMan.updateMap(activeMode);


        switch(activeMode) {
            case STOCK:
                stockMan.triggerNewRound();
                break;
            case PAINT:
                paintMan.triggerNewRound();
                break;
            case HUNT:
                huntMan.triggerNewRound();
                break;
        }
    }
    
    /** 
     * used to determine a random new mode when a round ends in PARTY mode 
     */
    private GameType randomModeGenerator() {
        GameType[] typeArray = {GameType.STOCK,GameType.HUNT,GameType.PAINT};
        Random random = new Random();
        int randomModeIndex = random.nextInt(3);
        GameType randomMode = typeArray[randomModeIndex];
        
        return randomMode;
    }
    
    public GameType getActiveMode() {
        return activeMode;
    }
    
    public StockModeManager getStockMan() {
        return this.stockMan;
    }
        
    public void setActiveMode(GameType type) {
        this.activeMode = type;
    }
}
