package cedarkart.managers.mode;

import cedarkart.managers.TrophyManager;
import java.util.ArrayList;
import cedarkart.managers.Item;
import cedarkart.managers.Item.ItemType;
import cedarkart.managers.HUDManager;
import cedarkart.managers.GameManager;
import cedarkart.managers.placement.PlacementManager;
import cedarkart.managers.placement.Placement;

public class HuntModeManager implements ModeManager {
 
    private TrophyManager trophyMan;
    private HUDManager HUDMan;
    private GameManager masterGameMan;
    private PlacementManager placementMan;
    
    HuntModeManager(GameManager gameMan) {
        this.masterGameMan = gameMan;
        this.trophyMan = masterGameMan.getTrophyMan();
        this.HUDMan = masterGameMan.getHUDMan();
        this.placementMan = masterGameMan.getPlacementMan();

    }
    
    /** 
     * called by GameModeMan to start a new Hunt Mode round 
     */
    @Override
    public void triggerNewRound(){        
                
        placementMan.removePlacement(placementMan.getMascotPlacement());
        ArrayList<Item> itemList = createItems();
        trophyMan.placeItemsAndBroadcast(itemList);
        String newObjective = "The Cedarville Mascot is hiding!  Be the first to find him!";
        HUDMan.updateObjective(newObjective);
        masterGameMan.sendObjective(newObjective);
    }
    
    /** 
     * creates an ArrayList of all new item placements for the new round 
     */
    private ArrayList<Item> createItems() {
        ArrayList<Item> huntList = new ArrayList<>();
        ArrayList<Placement> mascotPlacement;
        ItemType itemType;
        Item mascot;
        
        itemType = ItemType.MASCOT;
        mascotPlacement = placementMan.createMascotPlacement();
        mascot = new Item(mascotPlacement.get(0), itemType);
        
        huntList.add(mascot);
        return huntList;
    }
}
