package cedarkart.managers.mode;

import cedarkart.managers.GameManager;
import cedarkart.managers.HUDManager;
import cedarkart.managers.Item;
import cedarkart.managers.placement.Placement;
import cedarkart.managers.placement.PlacementManager;
import cedarkart.managers.SettingsManager;
import cedarkart.managers.TrophyManager;
import java.util.ArrayList;

public class PaintModeManager implements ModeManager {

    private TrophyManager trophyMan;
    private SettingsManager settingsMan;
    private HUDManager HUDMan;
    private GameManager masterGameMan;
    private PlacementManager placementMan;
    
    PaintModeManager(GameManager gameMan) {
        this.masterGameMan = gameMan;
        this.settingsMan = masterGameMan.getSettingsMan();
        this.trophyMan = masterGameMan.getTrophyMan();
        this.HUDMan = masterGameMan.getHUDMan();
        this.placementMan = masterGameMan.getPlacementMan();
    }
    
    /** 
     * called by GameModeMan to start a new Hunt Mode round 
     */
     @Override
     public void triggerNewRound(){
        
        ArrayList<Item> itemList = createItems(); //arrayList of single mascot item
        trophyMan.placeItemsAndBroadcast(itemList); 
        String newObjective = "It's time to paint the Rock!  Gather " + 
                settingsMan.getWinningPaintCount() + " paint cans and return "
                + "them to the Rock to win!";
        HUDMan.updateObjective(newObjective);
        masterGameMan.sendObjective(newObjective);
    }

     /** 
      * creates an ArrayList of all new item placements for the new round 
      */
    private ArrayList<Item> createItems() {
        ArrayList<Item> paintList = new ArrayList<>();
        ArrayList<Placement> paintPlacementList;
        ArrayList<Placement> rockPlacement;
        Item.ItemType itemType;
        
        itemType = Item.ItemType.DESTINATION;
        rockPlacement = placementMan.createRockPlacement();

        Item rockDestination = new Item(rockPlacement.get(0), itemType);
        paintList.add(rockDestination);

        itemType = Item.ItemType.PAINT_CAN;
        paintPlacementList = placementMan.createPaintCanPlacements();
        
        for(int i = 0; i < paintPlacementList.size(); i++) {
            Item paintCan = new Item(paintPlacementList.get(i), itemType);
            paintList.add(paintCan);
        }

        return paintList;
    }
}