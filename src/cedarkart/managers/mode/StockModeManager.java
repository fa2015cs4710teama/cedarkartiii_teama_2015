package cedarkart.managers.mode;

import cedarkart.managers.GameManager;
import cedarkart.managers.TrophyManager;
import cedarkart.managers.HUDManager;
import cedarkart.managers.Item;
import cedarkart.managers.placement.Placement;
import cedarkart.managers.placement.PlacementManager;
import java.util.ArrayList;

public class StockModeManager implements ModeManager {
    
    private TrophyManager trophyMan;
    private PlacementManager placementMan;
    private HUDManager HUDMan;
    private GameManager masterGameMan;
    private GameModeManager gameModeMan;
    private String RefillStationName;
    
    StockModeManager(GameManager gameMan) {
        this.masterGameMan = gameMan;
        this.trophyMan = masterGameMan.getTrophyMan();
        this.HUDMan = masterGameMan.getHUDMan();
        this.placementMan = masterGameMan.getPlacementMan();
    }
    
    /** 
     * called by GameModeMan to start a new Hunt Mode round 
     */
    @Override
    public void triggerNewRound(){

        ArrayList<Item> itemList = createItems(); //arrayList of single mascot item
        trophyMan.placeItemsAndBroadcast(itemList);   
        String newObjective = "The CedarPrint stations need refilling.  Hurry"
                + " to the Tyler Building to collect your paper!";
        HUDMan.updateObjective(newObjective);
        masterGameMan.sendObjective(newObjective);
    }
    
    /** 
     * creates an ArrayList of all new item placements for the new round 
     */
    private ArrayList<Item> createItems() {
        ArrayList<Item> stockList = new ArrayList<>();
        ArrayList<Placement> refillStationPlacement;
        ArrayList<Placement> paperPickupPlacement;
        Item.ItemType itemType;
        
        itemType = Item.ItemType.PAPER;
        paperPickupPlacement = placementMan.createPaperPickupPlacement();

        Item paperPickupDestination = new Item(paperPickupPlacement.get(0), itemType);
        stockList.add(paperPickupDestination);      

        itemType = Item.ItemType.DESTINATION;
        refillStationPlacement = placementMan.createRefillStationPlacement();
        this.RefillStationName = refillStationPlacement.get(0).getName().replace(" Item", "");

        Item refillStationDestination = new Item(refillStationPlacement.get(0), itemType);
        stockList.add(refillStationDestination);      
        
        return stockList;
    }
    
    public String getRefillStationName() {
        return this.RefillStationName;
    }
    
}
