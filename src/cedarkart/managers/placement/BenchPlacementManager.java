package cedarkart.managers.placement;

import com.jme3.math.Vector3f;
import java.util.ArrayList;

public class BenchPlacementManager {
    private static final float BENCH_MASS = 200f;
    private static final Vector3f BENCH_HITBOX = new Vector3f(0.51f, 0.325f, 0.21f);
    
    private static final float[][] BENCH_TRANSFORMS = {
        {-32.8f, 0.325f, 101.3f, 0.18f, 0.0000f, -0.5892f, 0.0000f, 0.8080f, 200f},
        {-33.6f, 0.325f, 105.9f, 0.18f, 0.0000f, 0.8828f, 0.0000f, -0.4696f, 200f},
        {-37.8f, 0.325f, 109.2f, 0.18f, 0.0000f, 0.9998f, 0.0000f, 0.0187f, 200f},
        {48.2f, 0.325f, 18.2f, 0.18f, 0.0000f, 0.7638f, 0.0000f, 0.6455f, 200f},
        {47.7f, 0.325f, 21.5f, 0.18f, 0.0000f, -0.1811f, 0.0000f, 0.9835f, 200f},
        {44.5f, 0.325f, 20.3f, 0.18f, 0.0000f, -0.2190f, 0.0000f, 0.9757f, 200f},
        {-4.3f, 0.325f, 15.2f, 0.18f, 0.0000f, 0.505f, 0.0000f, 0.8631f, 200f},
    }; 

    public static ArrayList<Placement> createBenchPlacements() {
        PlacementProperties carProperties = new PlacementProperties("Bench", 
        "Models/objects/bench/bench.mesh.xml", BENCH_MASS, 
        BENCH_HITBOX.x, BENCH_HITBOX.y, BENCH_HITBOX.z);
        
        return PlacementManager.placementArrayList(carProperties, BENCH_TRANSFORMS);
    }    
}
