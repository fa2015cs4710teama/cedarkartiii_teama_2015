package cedarkart.managers.placement;

import java.util.ArrayList;

public class BicyclePlacementManager {
    
    public static ArrayList<Placement> createBicyclePlacements() {
        ArrayList<Placement> placements = new ArrayList<>();

        placements.add(new Placement("Silver Bike 1", "Models/objects/bicycles/bike_silver/bike_silver.j3o", 13.5f, .369f, 162.6f, 0.13f, 0.0000f, 0.0707f, 0.0000f, 0.9975f, 150f, .5f, .369f, .1f));
        placements.add(new Placement("Green Bike 1", "Models/objects/bicycles/bike_green/bike_green.j3o", 12.8f, .369f, 162.8f, 0.13f, 0.0000f, 0.0707f, 0.0000f, 0.9975f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Blue Bike 1", "Models/objects/bicycles/bike_blue/bike_blue.j3o", 11.5f, .369f, 163.0f, 0.13f, 0.0000f, 0.0707f, 0.0000f, 0.9975f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Red Bike 1", "Models/objects/bicycles/bike_red/bike_red.j3o", 10.5f, .369f, 163.2f, 0.13f, 0.0000f, 0.0707f, 0.0000f, 0.9975f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Silver Bike 2", "Models/objects/bicycles/bike_silver/bike_silver.j3o", -53.049946f, .369f, 69.44986f, 0.13f, 0.0f, -0.60267645f, 0.0f, 0.7979857f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Green Bike 2", "Models/objects/bicycles/bike_green/bike_green.j3o", -53.049946f, .369f, 70.1f, 0.13f, 0.0f, -0.60267645f, 0.0f, 0.7979857f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Blue Bike 2", "Models/objects/bicycles/bike_blue/bike_blue.j3o", -52.549953f, .369f, 71.50f, 0.13f, 0.0f, -0.60267645f, 0.0f, 0.7979857f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Red Bike 2", "Models/objects/bicycles/bike_red/bike_red.j3o", -52.55f, .369f, 72.1f, 0.13f, 0.0f, -0.60267645f, 0.0f, 0.7979857f, 0f, 0f, 0f, 0f));

        return placements;
    }    
}
