package cedarkart.managers.placement;

import cedarkart.managers.SettingsManager.Level;
import java.util.ArrayList;

public class BuildingPlacementManager {
    
    public static ArrayList<Placement> createBuildingPlacements(Level lvl) {
        ArrayList<Placement> placements = new ArrayList<>();
        placements.add(new Placement("WRLD", "Models/buildings/worldBox/Cube.mesh.xml", 0f, 0f, 0f, 254.5f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("DMC", "Models/buildings/dmc/dmc.mesh.xml", 11f, 7f, 50f, 1.10f, 0f, -0.0699f, 0f, 0.9976f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("SSC", "Models/buildings/ssc/ssc.mesh.xml", -55f, 5.7f, 100f, 1.75f, 0f, 0.7726f, 0f, 0.6349f, 0f, 0f, 0f, 0f));
        // BTS WAS HERE
        placements.add(new Placement("ENS", "Models/buildings/ens/ens.mesh.xml", 4f, 6.5f, 172f, 3.95f, 0f, 0.0799f, 0f, 0.9968f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("MIL", "Models/buildings/milner/milner.mesh.xml", 37f, 7.0f, 155f, 0.70f, 0f, 0.0762f, 0f, 0.9971f, 0f, 0f, 0f, 0f));
        // APPLE WAS HERE
        placements.add(new Placement("LB", "Models/buildings/library/library.mesh.xml", 27f, 3.6f, 124.5f, 0.65f, 0f, -0.6910f, 0f, 0.7229f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("X", "Models/buildings/btsstairs/btsStairs.mesh.xml", 50f, 2.65f, 95.4f, 2.50f, 0f, -0.6372f, 0f, 0.7707f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("BRO", "Models/buildings/brock/brock.mesh.xml", 32f, 10.8f, -88f, 1.50f, 0f, 0.0810f, 0f, 0.9967f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("DFH", "Models/buildings/callan/callan.mesh.xml", -36f, 7.0f, 5f, 23f, 0f, -0.0853f, 0f, 0.9964f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("FH", "Models/buildings/founders/founders.mesh.xml", 92.1f, 11.2f, 187.0f, 1.03f, 0f, 0.0086f, 0f, 1f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("LAW", "Models/buildings/lawlorPrinty/lawlorPrinty.mesh.xml", 66f, 7.2f, 7f, 0.55f, 0f, 0.9956f, 0f, -0.0935f, 0f, 0f, 0f, 0f));//
        placements.add(new Placement("PR", "Models/buildings/lawlorPrinty/lawlorPrinty.mesh.xml", 31f, 7.2f, 4f, 0.55f, 0f, 0.1085f, 0f, 0.9941f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("MX", "Models/buildings/maddox/maddox.mesh.xml", 81f, 9.2f, 72f, 1.2f, 0f, 0.0810f, 0f, 0.9967f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("MCCH", "Models/buildings/mcchesney/mcchesney.mesh.xml", 9.7f, 7.8f, -30.5f, 4.50f, 0f, 0.1118f, 0f, 0.9337f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("TYL", "Models/buildings/tyler/tyler.obj", 53.6f, 7.9f, 195.5f, 3.4f, 0f, 0.7694f, 0f, 0.6388f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("PAT", "Models/buildings/patterson/patterson.obj", 111.1f, 8.5f, 147.8f, 2.48f, 0f, 0.0693f, 0f, 0.9976f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("WIL", "Models/buildings/willets/willets.obj", 61.2f, 6.0f, -33.2f, 0.81f, 0f, -0.6327f, 0f, 0.7744f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("WH", "Models/buildings/williams/williams.obj", 80.0f, 8.5f, 162.5f, 3.27f, 0f, 0.1005f, 0f, 0.9949f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("HRS", "Models/buildings/safety/safety.obj", 81.6f, 9.4f, 171.5f, 0.52f, 0f, 0.0984f, 0f, 0.9951f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("PHB", "Models/buildings/pharmacy/PHB.mesh.xml", -60f, 7.0f, 137f, 2f, 0f, -.4f, 0f, -0.2f, 0f, 0f, 0f, 0f));//subbed 8 from x,z
        
        if (lvl == Level.LOW) {
            placements.add(new Placement("BTS", "Models/buildings/bts/bts.mesh.xml", 56f, 2.55f, 95f, 2.50f, 0f, -0.6372f, 0f, 0.7707f, 0f, 0f, 0f, 0f));
            placements.add(new Placement("APP", "Models/buildings/apple/apple.mesh.xml", 77f, 9.0f, 123f, 6.10f, 0f, 0.9957f, 0f, -0.0925f, 0f, 0f, 0f, 0f));
        } else if (lvl == Level.MEDIUM) {
            placements.add(new Placement("BTS", "Models/buildings/bts_high/bts.mesh.xml", 56f, 2.55f, 95f, 2.50f, 0f, -0.6372f, 0f, 0.7707f, 0f, 0f, 0f, 0f));
            placements.add(new Placement("APP", "Models/buildings/apple_high/apple.mesh.xml", 77f, 8.9f, 123f, 6.10f, 0f, 0.9957f, 0f, -0.0925f, 0f, 0f, 0f, 0f));
        } else {
            placements.add(new Placement("BTS", "Models/buildings/bts_high/bts.j3o", 56f, 2.55f, 95f, 2.50f, 0f, -0.6372f, 0f, 0.7707f, 0f, 0f, 0f, 0f));
            placements.add(new Placement("APP", "Models/buildings/apple_high/apple.j3o", 77f, 8.9f, 123f, 6.10f, 0f, 0.9957f, 0f, -0.0925f, 0f, 0f, 0f, 0f));
        }

        return placements;
    }
}
