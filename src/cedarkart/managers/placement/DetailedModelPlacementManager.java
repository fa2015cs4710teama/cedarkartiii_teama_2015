package cedarkart.managers.placement;

import java.util.ArrayList;

public class DetailedModelPlacementManager {
    public static ArrayList<Placement> createDetailedModelPlacements() {
        ArrayList<Placement> placements = new ArrayList<>();
        placements.add(new Placement("Water Tower", "Models/objects/tower/tower.mesh.xml", -40.5f, 0f, -38f, 1.5f, 0f, 4f, 0f, 0f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Opened Eyes", "Models/objects/openedEyes/statue.mesh.xml", -10.8f, 0f, 89.5f, 1f, 0f, -0.1918f, 0f, 0f, 0f, 0f, 0f, 0f));

        return placements;
    }    
}
