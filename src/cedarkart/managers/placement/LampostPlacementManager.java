package cedarkart.managers.placement;

import com.jme3.math.Vector3f;
import java.util.ArrayList;

public class LampostPlacementManager {
    
    private static final float LAMP_POST_MASS = 500f;
    private static final Vector3f LAMP_POST_HITBOX = new Vector3f(.2f, 1.43f, .2f);
    
    private static float[][] basicLampPostTransforms = {
        {21.1f, 1.435f, 23.0f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {9.9f, 1.435f, 18.8f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {2.7f, 1.435f, 30.7f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {-14.2f, 1.435f, 33.2f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {32.0f, 1.435f, 20.5f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {40.6f, 1.435f, 143.7f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {41.2f, 1.435f, 102.8f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {31.8f, 1.435f, 109.2f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {21.3f, 1.435f, 114.7f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {13.3f, 1.435f, 123.7f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {9.9f, 1.435f, 134.9f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {9.7f, 1.435f, 146.5f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {34.7f, 1.435f, -55.0f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {36.5f, 1.435f, -45.2f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {38.9f, 1.435f, -33.2f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {40.7f, 1.435f, -23.2f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {43.1f, 1.435f, -11.8f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {45.7f, 1.435f, 1.0f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {47.9f, 1.435f, 12.4f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {49.7f, 1.435f, 21.8f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {47.0f, 1.435f, 112.6f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {50.4f, 1.435f, 128.2f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {55.8f, 1.435f, 139.0f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {57.6f, 1.435f, 148.4f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {59.5f, 1.435f, 158.4f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {61.5f, 1.435f, 169.0f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {63.5f, 1.435f, 177.8f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},
        {65.5f, 1.435f, 189.0f, 2.2f, 0.0000f, 0.0000f, 0.0000f, 0.0000f},            
    };
    
    private static float[][] bannerLampPostTransforms = {
        {-43.8f, 1.435f, 115.3f, 2.2f, 0.0000f, -0.1680f, 0.0000f, 0.9858f},
        {-46.6f, 1.435f, 123.1f, 2.2f, 0.0000f, -0.1680f, 0.0000f, 0.9858f},
        {-43.2f, 1.435f, 130.9f, 2.2f, 0.0000f, 0.5028f, 0.0000f, 0.8644f},
        {-36.0f, 1.435f, 134.7f, 2.2f, 0.0000f, 0.5028f, 0.0000f, 0.8644f},
        {-29.0f, 1.435f, 138.5f, 2.2f, 0.0000f, 0.5028f, 0.0000f, 0.8644f},
        {-17.6f, 1.435f, 144.5f, 2.2f, 0.0000f, 0.5028f, 0.0000f, 0.8644f},
        {-10.8f, 1.435f, 148.1f, 2.2f, 0.0000f, 0.5028f, 0.0000f, 0.8644f},
        {-3.8f, 1.435f, 151.7f, 2.2f, 0.0000f, 0.4695f, 0.0000f, 0.8829f},
    };
    
    static ArrayList<Placement> createLampPostPlacements(boolean isNight) {
        if (isNight) {
            return createLampPostPlacements("Models/objects/lamppost_v2/lamppost_v2_noglass.mesh.xml", 
                    "Models/objects/lamppost_v2_banner/lamppost_v2_banner_noglass.mesh.xml");
        } else {
            return createLampPostPlacements("Models/objects/lamppost_v2/lamppost_v2.mesh.xml", 
                    "Models/objects/lamppost_v2_banner/lamppost_v2_banner.mesh.xml");
        }
    }

    private static ArrayList<Placement> createLampPostPlacements(String basicLampPostPath, String bannerLampPostPath) {
        PlacementProperties basicProperties = new PlacementProperties("LampPost", 
                basicLampPostPath, LAMP_POST_MASS, 
                LAMP_POST_HITBOX.x, LAMP_POST_HITBOX.y, LAMP_POST_HITBOX.z);
        
        ArrayList<Placement> lampPostPlacements = PlacementManager.placementArrayList(basicProperties, basicLampPostTransforms);
        
        PlacementProperties bannerProperties = new PlacementProperties("LampPostBanner", 
                bannerLampPostPath, LAMP_POST_MASS, 
                LAMP_POST_HITBOX.x, LAMP_POST_HITBOX.y, LAMP_POST_HITBOX.z);
        
        lampPostPlacements.addAll(PlacementManager.placementArrayList(bannerProperties, bannerLampPostTransforms));
        
        return lampPostPlacements;
    }

    static ArrayList<Placement> createLampGlowPlacements() {
        float LIGHT_HEIGHT_ABOVE_LAMP = 6.135f;
        
        PlacementProperties basicProperties = new PlacementProperties("LampGlow", 
                "Models/objects/lamppost_v2/lamppost_v2glowpart.j3o", 0f, 
                0f, 0f, 0f);
        
        float[][] transforms = new float[basicLampPostTransforms.length + bannerLampPostTransforms.length][];
        System.arraycopy(basicLampPostTransforms, 0, transforms, 0, basicLampPostTransforms.length);
        System.arraycopy(bannerLampPostTransforms, 0, transforms, basicLampPostTransforms.length, bannerLampPostTransforms.length);
        
        for(int i = 0; i < transforms.length; i++) {
            transforms[i][1] += LIGHT_HEIGHT_ABOVE_LAMP;
        }
        
        return PlacementManager.placementArrayList(basicProperties, transforms);
    }
}
