package cedarkart.managers.placement;

import java.util.ArrayList;

public class MiscMoveableObjectPlacementManager {
    public static ArrayList<Placement> createMoveablePlacements() {
        ArrayList<Placement> placements = new ArrayList<>();
        placements.add(new Placement("Yellow Jacket", "Models/people/jacket/jacket.mesh.xml", -30.0f, .8f, 92f, 1.5f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));

        return placements;
    }    
}
