package cedarkart.managers.placement;

import java.util.ArrayList;

public class MiscStaticObjectPlacementManager {
    
    public static ArrayList<Placement> createObjectPlacements() {
        ArrayList<Placement> placements = new ArrayList<>();
        placements.add(new Placement("DMC Bridge", "Models/objects/bridge_dmc_tex/bridge_dmc_tex.mesh.xml", 43.5f, 7.2f, 71.5f, 2.8f, 0f, 0.41f, 0f, 0.9260f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("ENS Bridge", "Models/objects/bridge/bridge.obj", -22.3f, 7.1f, 140.3f, 0.40f, 0f, 0.5120f, 0f, 0.8590f, 0f, 0f, 0f, 0f));//-.3
        placements.add(new Placement("Spillway", "Models/objects/Spillway/spillway.mesh.xml", -22.2f, 6.47f, 140.2f, 1.8f, 0f, 1.5f, 0f, .38f, 0f, 0f, 0f, 0f));//-.2
        //placements.add(new Placement("Finish", "Models/objects/finishLine/finishLine.obj", -28.0f, 6.5f, 92.5f, 1f, 0.0000f, 0.8499f, 0.0000f, 0.5270f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("SSC Clock", "Models/objects/sscClock/sscClock.obj", -38.1f, 7.0f, 104.1f, 0.15f, 0.0000f, 0.0000f, 0.0000f, 0.0000f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Creation 1", "Models/objects/creation/creation.obj", -32.0f, 7.3f, 101.0f, 0.13f, 0.0000f, -0.5378f, 0.0000f, 0.8431f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Creation 2", "Models/objects/creation/creation.obj", -31.5f, 7.3f, 103.0f, 0.13f, 0.0000f, -0.7249f, 0.0000f, 0.6888f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Creation 3", "Models/objects/creation/creation.obj", -32.0f, 7.3f, 105.0f, 0.13f, 0.0000f, -0.8188f, 0.0000f, 0.5741f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Creation 4", "Models/objects/creation/creation.obj", -33.0f, 7.3f, 107.0f, 0.13f, 0.0000f, 0.8749f, 0.0000f, -0.4843f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Creation 5", "Models/objects/creation/creation.obj", -34.5f, 7.3f, 108.5f, 0.13f, 0.0000f, 0.9477f, 0.0000f, -0.3193f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Creation 6", "Models/objects/creation/creation.obj", -36.5f, 7.3f, 110.0f, 0.13f, 0.0000f, 0.9689f, 0.0000f, -0.2475f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Creation 7", "Models/objects/creation/creation.obj", -38.5f, 7.3f, 111.0f, 0.13f, 0.0000f, 0.9929f, 0.0000f, -0.1192f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Rock", "Models/objects/rock/rock.obj", -22.0f, 7.3f, 93.0f, 2.7f, 0.0000f, 0.8499f, 0.0000f, 0.5270f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Lake Wall 1", "Models/objects/lakeWall/lakeWall.obj", -9.59998f, 4.5f, 92.7f, 0.53f, 0.0000f, 0.2532f, 0.0000f, 0.9674f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Lake Wall 2", "Models/objects/lakeWall/lakeWall.obj", -16.25f, 4.5f, 96.45f, 0.53f, 0.0000f, 0.2532f, 0.0000f, 0.9674f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Lake Wall 3", "Models/objects/lakeWall/lakeWall.obj", 9.7f, 4.5f, 115.3f, 0.53f, 0.0000f, 0.5191f, 0.0000f, 0.8547f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Lake Wall 4", "Models/objects/lakeWall/lakeWall.obj", 6.18f, 4.5f, 122.08f, 0.53f, 0.0000f, 0.5191f, 0.0000f, 0.8547f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("BTS Statue", "Models/objects/btsStatue/btsStatue.obj", 54.5f, 7.8f, 83.1f, 0.55f, 0.0000f, 0.8663f, 0.0000f, -0.4995f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Flag Pole", "Models/objects/flagpole/flagpole.mesh.xml", 13.3f, 6.8f, 116.7f, 2f, 0f, -0.35f, 0f, -0.75f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Bent", "Models/objects/bent/bent.mesh.xml", 7.29f, 7.8f, 152.85f, .8f, 0f, 0.8660f, 0f, -.5f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Bike Rack 1", "Models/objects/bicycles/bikerack/bikerack.obj", 10.6f, 8.15f, 163.4f, 0.19f, 0.0000f, 0.0911f, 0.0000f, 0.9958f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Bike Rack 2", "Models/objects/bicycles/bikerack/bikerack.obj", 12.8f, 8.15f, 163.0f, 0.19f, 0.0000f, 0.0911f, 0.0000f, 0.9958f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Bike Rack 3", "Models/objects/bicycles/bikerack/bikerack.obj", -52.949947f, 9.6f, 72.150024f, 0.19f, 0.0f, 0.75544006f, 0.0f, 0.6552179f, 0f, 0f, 0f, 0f));
        placements.add(new Placement("Bike Rack 4", "Models/objects/bicycles/bikerack/bikerack.obj", -53.49994f, 9.6f, 70.1499f, 0.19f, 0.0f, 0.75544006f, 0.0f, 0.6552179f, 0f, 0f, 0f, 0f));

        return placements;
    }    
}
