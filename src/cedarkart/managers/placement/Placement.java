
package cedarkart.managers.placement;

import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.network.serializing.Serializable;
import java.lang.annotation.Annotation;

/**
 * Contains all the placement info for a building
 * @author David Riggleman
 */
@Serializable
public class Placement implements Serializable {

    public static final Float BUILDING_SCALE = 1f;
    public static final Float BASE_MASS = 1f;
    public static final Float LOCATION_SCALE = 1f;
    public static final Float TRANSLATION_X = 0f;
    public static final Float TRANSLATION_Y = 0f;
    public static final Float TRANSLATION_Z = 0f;
    
    String name;
    String filePath;
    float locationX, locationY, locationZ;
    float scale;
    float rotationX, rotationY, rotationZ, rotationW;
    float mass;
    float hitboxX, hitboxY, hitboxZ;
    float fiction = -1.0f;
    
    public Placement() {
        
    }
    
    Placement(String name, String filePath, float locationX, float locationY,
            float locationZ, float scale, float rotationX, float rotationY,
            float rotationZ, float rotationW, float mass, float hitboxX,
            float hitboxY, float hitboxZ){
        this.name = name;
        this.filePath = filePath;
        this.locationX = locationX;
        this.locationY = locationY;
        this.locationZ = locationZ;
        this.scale = scale;
        this.rotationX = rotationX;
        this.rotationY = rotationY;
        this.rotationZ = rotationZ;
        this.rotationW = rotationW;
        this.mass = mass;
        this.hitboxX = hitboxX;
        this.hitboxY = hitboxY;
        this.hitboxZ = hitboxZ;
    }
    
    @Override
    public boolean equals(Object other) {
        if(!(other instanceof Placement)) {
            return false;
        }
        Placement otherPlacement = (Placement) other;
        return (this.name == null ? otherPlacement.getName() == null : this.name.equals(otherPlacement.getName()));
    }

    public Vector3f getLocalTranslation(){
        float x = locationX * LOCATION_SCALE + TRANSLATION_X;
        float y = locationY + TRANSLATION_Y;
        float z = locationZ * LOCATION_SCALE + TRANSLATION_Z;
        
        return new Vector3f(x, y, z);
    }
    
    public Vector3f getLocation() {
        return new Vector3f(locationX, locationY, locationZ);
    }
    
    public Vector3f getHitbox(){
        float x = hitboxX;
        float y = hitboxY;
        float z = hitboxZ;
        
        return new Vector3f(x, y, z);
    }
    
    public float getScale(){
        return scale * BUILDING_SCALE;
    }
    
    public float getMass(){
        return mass * BASE_MASS;
    }
    
    public String getName(){
        return name;
    }
    
    public Quaternion getRotation(){
        Quaternion rotation = new Quaternion(rotationX, rotationY, rotationZ, rotationW); 
        return rotation;
    }

    @Override
    public Class serializer() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public short id() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Class<? extends Annotation> annotationType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
