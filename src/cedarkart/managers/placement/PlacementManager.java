package cedarkart.managers.placement;

import cedarkart.Counter;
import cedarkart.managers.ICedarKartManager;
import cedarkart.managers.PhysicsManager;
import cedarkart.managers.SettingsManager;
import cedarkart.managers.SettingsManager.Weather;
import cedarkart.managers.WorldManager;
import com.jme3.asset.AssetManager;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Transform;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.LightNode;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.shader.VarType;
import com.jme3.terrain.geomipmap.TerrainQuad;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author nabond
 */
public class PlacementManager implements ICedarKartManager {

    // Standard Manangers
    private final AssetManager assetManager;
    // Custom Manangers
    private final WorldManager worldMan;
    private final PhysicsManager physicsMan;
    private final SettingsManager settingsMan;
    private TerrainQuad mapTerrain;
    private final Node rootNode;
    private final Counter nextPlayerLoc = new Counter();
    private final ArrayList<Vector3f> playerLocs = new ArrayList<>();
    private final ArrayList<Float> playerRots = new ArrayList<>();
    //trophy stuff
    private boolean directionUp = true;
    private float trophyHeight;

    public PlacementManager(AssetManager assetManager, Node rootNode,
            WorldManager worldMan, SettingsManager settingsMan) {
        this.assetManager = assetManager;
        this.worldMan = worldMan;
        this.physicsMan = worldMan.getPhysicsManager();
        this.settingsMan = settingsMan;
        this.rootNode = rootNode;
        
        if (this.playerLocs.size() != this.playerRots.size()) {
            throw new UnsupportedOperationException("There must be the same"
                    + " number of player locations as player rotations.  Check"
                    + " PlacementManager.initPlayerLocs() and"
                    + " PlacementManager.initPlayerRots().");
        }
    }
    
    public void start(TerrainQuad terrain) {
        //setWorldBox(terrain);
        this.mapTerrain = terrain;
         addPlacements(BuildingPlacementManager
                .createBuildingPlacements(this.settingsMan.getLevel()), true);
        addPlacements(MiscStaticObjectPlacementManager.createObjectPlacements(), true);

        if (settingsMan.getWeather().equals(Weather.NIGHT)) {
            addLamps(LampostPlacementManager.createLampPostPlacements(true), 
                    LampostPlacementManager.createLampGlowPlacements(),
                    true, terrain, settingsMan.advancedLighting());
        } else {
            addMoveablePlacements(LampostPlacementManager.createLampPostPlacements(false), true, terrain);
        }

        boolean isWinter = settingsMan.getWeather().equals(Weather.WINTER);
        addMoveablePlacements(TreePlacementManager.createTreePlacements(isWinter), true, terrain);
        if (isWinter) {
            addPlacements(WinterPlacementManager.createWinterDetailPlacements(), true);
        }

        addAutoHeightPlacements(BicyclePlacementManager.createBicyclePlacements(), true, terrain);

        addMoveablePlacements(VehiclePlacementManager.createVehiclePlacements(), true, terrain);
        addMoveablePlacements(BenchPlacementManager.createBenchPlacements(), true, terrain);
        // water tower and bee origin not at base so can't auto-height
        addAutoHeightPlacements(DetailedModelPlacementManager.createDetailedModelPlacements(), true, terrain);
        addMoveablePlacements(MiscMoveableObjectPlacementManager.createMoveablePlacements(), true, terrain);

        initPlayerLocs(terrain);
        initPlayerRots();
    }
    
    //This one is a little different in how the Trophies are dealt with:
    //The names of the trophies should be the base names of the Texture files
    //The filepath is the path where the textures are to be found.
    public ArrayList<Placement> createTrophyPlacements() {
        ArrayList<Placement> placements = new ArrayList<>();

        placements.add(new Placement("Shomper", "Models/trophies/profs/", 8.58f, 9.08f, 161.36833f, .5f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Schumacher", "Models/trophies/profs/", 65.49f, 8.28f, -34.76f, .5f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Gallagher", "Models/trophies/profs/", 10.19f, 8.66f, -32.77f, .5f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Hamman", "Models/trophies/profs/", -33.69f, 8.05f, 104.86f, .5f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Kohl", "Models/trophies/profs/", 46.08f, 7.87f, 90.30f, .5f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Fang", "Models/trophies/profs/", -20.66f, 8.65f, 140.98f, .5f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Shomper", "Models/trophies/profs/", 44.66f, 9.11f, 159.73f, .5f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Schumacher", "Models/trophies/profs/", 81.49f, 9.14f, 93.52f, .5f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Gallagher", "Models/trophies/profs/", -37.62f, 8.80f, 1.71f, .5f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Hamman", "Models/trophies/profs/", -59.88f, 8.10f, 53.60f, .5f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));

        return placements;
    }

    public ArrayList<Placement> createPowerUpPlacements() {
        ArrayList<Placement> placements = new ArrayList<>();

        placements.add(new Placement("Power1", "Models/trophies/powers/", 43.20f, 6.61f, 71.51f, .2f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Power2", "Models/trophies/powers/", 12.71f, 7.38f, 116.03f, .2f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Power3", "Models/trophies/powers/", -27.13f, 5.45f, 149.20f, .2f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Power4", "Models/trophies/powers/", -0.30f, 8.08f, 72.66f, .2f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Power5", "Models/trophies/powers/", 35.89f, 7.96f, 58.10f, .2f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Power6", "Models/trophies/powers/", 72.70f, 8.37f, -4.81f, .2f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Power7", "Models/trophies/powers/", 24.74f, 8.15f, 16.26f, .2f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Power8", "Models/trophies/powers/", 55.23f, 7.49f, 83.42f, .2f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Power9", "Models/trophies/powers/", 44.17f, 8.33f, 197.06f, .2f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));
        placements.add(new Placement("Power10", "Models/trophies/powers/", -54.30f, 9.82f, 72.20f, .2f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f));

        return placements;
    }
    
    public static ArrayList<Placement> placementArrayList(PlacementProperties properties, float[][] transforms) {
        ArrayList<Transform> ALTransforms = new ArrayList<>();
        
        for(int i = 0; i < transforms.length; i++) {
            float[] trans = transforms[i];
            Vector3f location = new Vector3f(trans[0], trans[1], trans[2]);
            Vector3f scale = new Vector3f(trans[3], 0, 0);
            Quaternion rotation = new Quaternion(trans[4], trans[5], trans[6], trans[7]);
            
            ALTransforms.add(new Transform(location, rotation, scale));
        }
        
        return placementArrayList(properties, ALTransforms);
    }
    
    public static ArrayList<Placement> placementArrayList(PlacementProperties properties, ArrayList<Transform> transforms) {
        ArrayList<Placement> placements = new ArrayList<>();
        
        String basename = properties.getName();
        String filePath = properties.getFilePath();
        float mass = properties.getMass();
        Vector3f hitBox = properties.getHitbox();
        float hitBoxX = hitBox.x;
        float hitBoxY = hitBox.y;
        float hitBoxZ = hitBox.z;
        
        for(int i = 0; i < transforms.size(); i++) {
            Transform cTransform = transforms.get(i);
            
            Vector3f location = cTransform.getTranslation();
            float locX = location.x;
            float locY = location.y;
            float locZ = location.z;
            
            Quaternion rotation = cTransform.getRotation();
            float rotX = rotation.getX();
            float rotY = rotation.getY();
            float rotZ = rotation.getZ();
            float rotW = rotation.getW();
            
            Vector3f scaleV = cTransform.getScale();
            float scale = scaleV.x + scaleV.y + scaleV.z;
            
            placements.add(new Placement(basename + i, filePath, locX, locY, locZ, scale, rotX, rotY, rotZ, rotW, mass, hitBoxX, hitBoxY, hitBoxZ));
        }
        
        return placements;
    }
    
    private void fixAmbient(Spatial spatial) {
        if (spatial instanceof Node) {
            Node node = (Node) spatial;
            for (int i = 0; i < node.getQuantity(); i++) {
                Spatial child = node.getChild(i);
                fixAmbient(child);
            }
        } else if (spatial instanceof Geometry) {
            ((Geometry) spatial).getMaterial().setParam("UseMaterialColors",
                    VarType.Boolean, false);
        }
    }

    public void addPlacements(ArrayList<Placement> placements, boolean addPhysics) {
        for (Placement placement : placements) {
            Spatial spatial = (Spatial) assetManager.loadModel(placement.filePath);
            spatial.setLocalTranslation(placement.getLocalTranslation());
            spatial.setLocalScale(placement.getScale());
            spatial.setLocalRotation(placement.getRotation());
            spatial.setName(placement.getName());
            spatial.setShadowMode(ShadowMode.CastAndReceive);
            rootNode.attachChild(spatial);
            
            if (placement.filePath.endsWith(".j3o")) {
                fixAmbient(spatial);
            }
            
            if (addPhysics) {
                if (placement.fiction < 0.0f) {
                    physicsMan.addMeshPhysics(spatial);
                } else {
                    physicsMan.addMeshFrictionPhysics(spatial, placement.fiction);
                }
            }
            worldMan.putSpatial(placement.getName(), spatial);
        }
    }
    
    public void addTrophyPlacements(ArrayList<Placement> placements, boolean addPhysics) {
        for (Placement placement : placements) {
            //Spatial spatial = (Spatial) assetManager.loadModel(placement.filePath);
            Box b = new Box(1f, 1f, 1f);
            Geometry geom = new Geometry(placement.getName(), b);
            geom.setLocalTranslation(placement.getLocalTranslation());
            geom.setLocalScale(placement.getScale());
            geom.setLocalRotation(placement.getRotation());
            trophyHeight = geom.getLocalTranslation().y;
            
            Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
            
                int id = Integer.parseInt(placement.getName().substring("Power".length()));
                id = (id % 6) + 1;
                
                mat.setTexture("ColorMap", assetManager.loadTexture(placement.filePath
                        + id + ".png"));
                mat.setTexture("GlowMap", assetManager.loadTexture(placement.filePath
                        + id + "Glow.png"));

            geom.setMaterial(mat);

            rootNode.attachChild(geom);
            
            if (placement.filePath.endsWith(".j3o")) {
                fixAmbient(geom);
            }
            
            if (addPhysics) {
                physicsMan.addMeshPhysics(geom);
            }
            worldMan.putSpatial(placement.getName(), geom);
        }
    }
    
    private void addAutoHeightPlacements(ArrayList<Placement> placements,
            boolean addPhysics, TerrainQuad terrain) {
        for (Placement placement : placements) {
            Spatial spatial = (Spatial) assetManager.loadModel(placement.filePath);

            // get the height of the terrain to set the object at
            Vector3f location3d = placement.getLocalTranslation();
            Vector2f location2d = new Vector2f(location3d.x, location3d.z);
            location3d.y = terrain.getHeight(location2d) + WorldManager.WORLD_OFFSET
                    + placement.locationY;

            spatial.setLocalTranslation(location3d);
            spatial.setLocalScale(placement.getScale());
            spatial.setLocalRotation(placement.getRotation());
            spatial.setName(placement.getName());
            spatial.setShadowMode(ShadowMode.CastAndReceive);
            rootNode.attachChild(spatial);
            
            if (placement.filePath.endsWith(".j3o")) {
                fixAmbient(spatial);
            }
            
            if (addPhysics) {
                physicsMan.addMeshPhysics(spatial);
            }
            worldMan.putSpatial(placement.getName(), spatial);
        }
    }

    private void addMoveablePlacements(ArrayList<Placement> placements,
            boolean addPhysics, TerrainQuad terrain) {
        for (Placement placement : placements) {
            Spatial spatial = (Spatial) assetManager.loadModel(placement.filePath);

            // get the height of the terrain to set the object at
            Vector3f location3d = placement.getLocalTranslation();
            Vector2f location2d = new Vector2f(location3d.x, location3d.z);
            location3d.y = terrain.getHeight(location2d) + WorldManager.WORLD_OFFSET
                    + placement.locationY;

            spatial.setLocalTranslation(location3d);
            spatial.setLocalScale(placement.getScale());
            spatial.setLocalRotation(placement.getRotation());
            spatial.setName(placement.getName());
            spatial.setShadowMode(ShadowMode.CastAndReceive);
            rootNode.attachChild(spatial);
            
            if (placement.filePath.endsWith(".j3o")) {
                fixAmbient(spatial);
            }
            
            if (addPhysics) {
                physicsMan.addBoxPhysics(spatial, placement.getMass(), placement.getHitbox());
            }
            worldMan.putSpatial(placement.getName(), spatial);
        }
    }

    private void addLamps(ArrayList<Placement> lamps, ArrayList<Placement> lights,
            boolean addPhysics, TerrainQuad terrain, boolean addPointLights) {
        Material glowMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        glowMat.setColor("Color", ColorRGBA.White);
        glowMat.setColor("GlowColor", ColorRGBA.White);
        for (int i = 0; i < lamps.size(); i++) {
            Placement lamp = lamps.get(i);
            Placement light = lights.get(i);
            Node pivot = new Node("lampNode");

            Spatial spatial = (Spatial) assetManager.loadModel(lamp.filePath);
            Spatial lightObject = (Spatial) assetManager.loadModel(light.filePath);

            // get the height of the terrain to set the object at
            Vector3f location3d = lamp.getLocalTranslation();
            Vector2f location2d = new Vector2f(location3d.x, location3d.z);
            location3d.y = terrain.getHeight(location2d) + WorldManager.WORLD_OFFSET;


            pivot.setLocalTranslation(location3d.x, location3d.y + lamp.locationY, location3d.z);
            pivot.attachChild(spatial);
            pivot.attachChild(lightObject);

            if (addPointLights) {
                PointLight pl = new PointLight();
                pl.setRadius(7f);
                LightNode lNode = new LightNode("pointlight", pl);
                lNode.setLocalTranslation(0f, 1.5f, 0f);
                pivot.attachChild(lNode);
                rootNode.addLight(pl);
            }

            spatial.setLocalTranslation(0, 0, 0);
            spatial.setLocalScale(lamp.getScale());
            spatial.setName(lamp.getName());
            //spatial.setLocalRotation(lamp.getRotation());
            spatial.setShadowMode(ShadowMode.CastAndReceive);
            pivot.attachChild(spatial);

            lightObject.setLocalTranslation(0, -lamp.locationY, 0);
            lightObject.setLocalScale(light.getScale());
            lightObject.setName(light.getName());
            //lightObject.setLocalRotation(light.getRotation());
            pivot.attachChild(lightObject);

            //give glow to lamps if night
            lightObject.setMaterial(glowMat);

            rootNode.attachChild(pivot);

            if (lamp.filePath.endsWith(".j3o")) {
                fixAmbient(spatial);
            }

            if (addPhysics) {
                physicsMan.addNodeBoxPhysics(pivot, lamp.getMass(), lamp.getHitbox());
            }
        }
    }
    
    public Vector3f getHeightAtLocation(float x, float z, TerrainQuad terrain) {
        float y = terrain.getHeight(new Vector2f(x, z)) + WorldManager.WORLD_OFFSET;
        return new Vector3f(x, y, z);
    }

    // removes all the objects from the placement array from the game world
    public void removeObjects(ArrayList<Placement> placements) {
        for (Placement placement : placements) {
            Spatial spatial = rootNode.getChild(placement.getName());
            if (spatial != null) {
                spatial.removeFromParent();
            }
        }
    }
    
    /*
     * initPlayerLos() and initPlayerRots() work in tandem to create a series of
     * player init location and rotations.  This is designed for use with init'ing
     * the locatino of multiple players, such that each players location is
     * specified.  This could be a series of "spawn points" for a free-foam
     * style game or the start location of each player with a race type game.
     * 
     * It also allows for retreiving the start point closest to a given location.
     */

    private void initPlayerLocs(TerrainQuad terrain) {
        final float HEIGHT = 5.0f;
        this.playerLocs.clear();
        
        this.playerLocs.add(new Vector3f(28.6f, HEIGHT, 113.2f));
        
        for (Vector3f vector: this.playerLocs) {
            vector.y += terrain.getHeight(new Vector2f(vector.x, vector.z)) + WorldManager.WORLD_OFFSET;
        }
    }    
    public Vector3f playerLocation(int id) {
        return this.playerLocs.get(id % this.playerLocs.size());
    }
    
    public Vector3f nearestResetLocation(Vector3f currLoc) {
        Vector3f nearest = null;
        float distance = Float.MAX_VALUE;
        
        for (Vector3f vector: this.playerLocs) {
            float calcDis = (float) (FastMath.sqr(currLoc.x - vector.x)
                    + FastMath.sqr(currLoc.z - vector.z));
            if (calcDis < distance) {
                nearest = vector;
                distance = calcDis;
            }
        }
        
        return nearest;
    }
    
    public Vector3f randomResetPoint() {
        Random random = new Random(System.currentTimeMillis());
        return this.playerLocs.get(random.nextInt() % this.playerLocs.size());
    }

    private void initPlayerRots() {
        this.playerRots.clear();
        this.playerRots.add(3.4f);
    }
    
    public Quaternion playerRotation(int id) {
        Quaternion rotation = new Quaternion();
        float angle = this.playerRots.get(id % this.playerRots.size());
        rotation.fromAngleAxis(-FastMath.PI/angle, new Vector3f(0, 1, 0));
        return rotation;
    }
    
    public Quaternion rotationForLocation(Vector3f loc) {
        int index = this.playerLocs.indexOf(loc);
        Quaternion rotation = new Quaternion();
        float angle = this.playerRots.get(index);
        rotation.fromAngleAxis(-FastMath.PI/angle, new Vector3f(0, 1, 0));
        return rotation;
    }

    @Override
    public void start() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void rotateObjects(ArrayList<Placement> placements) {
        for (Placement placement : placements) {
            Spatial spatial = rootNode.getChild(placement.getName());
            spatial.rotate(rotateZ());
        }
    }

    private Quaternion rotateZ() {
        Quaternion pitch = new Quaternion();
        pitch.fromAngleAxis(FastMath.PI * .2f / 180.0f, new Vector3f(0.0f,
                1.0f, 0.0f));
        return pitch;
    }

    public void bounceObjects(ArrayList<Placement> placements, float count) {
        for (Placement placement : placements) {
            Spatial spatial = rootNode.getChild(placement.getName());
            Vector3f location = spatial.getLocalTranslation();
            location.y = location.y + ((float)Math.sin(count / 10) / 200); //was 300/1000
            
        }
    }
    
//    public void setWorldBox(TerrainQuad terrain){
//        float size = terrain.getTerrainSize()*worldMan.WORLD_SCALE;
//        Box b = new Box(size, size, size);
//        Geometry geom = new Geometry("WorldBox", b);
//        Material mat = new Material(this.assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
//        mat.setColor("Color", new ColorRGBA(0.0f, 0.0f, 0.0f, 0.0f));
//        mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
//        geom.setMaterial(mat);
//        
//        rootNode.attachChild(geom);
//        physicsMan.addBoxPhysics(geom, 0, new Vector3f(size,size,size));
//        //geom.setCullHint(Spatial.CullHint.Always);
//    } 

    private ArrayList<Placement> createMoveablePlacements() {
        ArrayList<Placement> placements = new ArrayList<>();
        placements.add(getMascotPlacement());

        return placements;
    }

    public Placement getMascotPlacement() {
        return new Placement("Yellow Jacket", "Models/people/jacket/jacket.mesh.xml", -30.0f, .8f, 92f, 1.5f, 0.0000f, 0f, 0f, 0.5270f, 250f, .2f, .85f, .2f);
    }
    
    public void replaceMascot() {
        addMoveablePlacements(createMoveablePlacements(), true, this.mapTerrain);
    }
    
    /** 
     * creates a random selection of paint can placements 
     */
    public ArrayList<Placement> createPaintCanPlacements() {
        Placement randomPlacements[] = new Placement[20];
        ArrayList<Placement> chosenPlacements = new ArrayList<>();       
        randomPlacements[0] = new Placement("Paint Can Item 0", "Models/objects/paintBucket/PaintBucket.mesh.xml", -40.256264f, 8.417032f, -32.82083f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //water tower
        randomPlacements[1] = new Placement("Paint Can Item 1", "Models/objects/paintBucket/PaintBucket.mesh.xml", -73.87149f, 8.661251f, 51.869083f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //behind ssc
        randomPlacements[2] = new Placement("Paint Can Item 2", "Models/objects/paintBucket/PaintBucket.mesh.xml", 90.76696f, 9.5f, 175.35458f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //founders
        randomPlacements[3] = new Placement("Paint Can Item 3", "Models/objects/paintBucket/PaintBucket.mesh.xml", 93.847755f, 9.0f, 82.442f,  1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //behind maddox 
        randomPlacements[4] = new Placement("Paint Can Item 4", "Models/objects/paintBucket/PaintBucket.mesh.xml", 51.473324f, 9.2f, 185.77617f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //tyler
        randomPlacements[5] = new Placement("Paint Can Item 5", "Models/objects/paintBucket/PaintBucket.mesh.xml", 72.71669f, 9.2f, 144.21117f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //field behind BTS
        randomPlacements[6] = new Placement("Paint Can Item 6", "Models/objects/paintBucket/PaintBucket.mesh.xml", -51.438152f, 8.403106f, 150.82689f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //field next to hsc 
        randomPlacements[7] = new Placement("Paint Can Item 7", "Models/objects/paintBucket/PaintBucket.mesh.xml", 7.551855f, 9.2f, 160.89159f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //in front of ens
        randomPlacements[8] = new Placement("Paint Can Item 8", "Models/objects/paintBucket/PaintBucket.mesh.xml", 49.538853f, 9.2f, 164.45004f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //treed milner area 
        randomPlacements[9] = new Placement("Paint Can Item 9", "Models/objects/paintBucket/PaintBucket.mesh.xml", 33.83422f, 8.417032f, -79.62378f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //brock
        randomPlacements[10] = new Placement("Paint Can Item 10", "Models/objects/paintBucket/PaintBucket.mesh.xml", 8.181036f, 8.417032f, -32.238483f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //Miter center
        randomPlacements[11] = new Placement("Paint Can Item 11", "Models/objects/paintBucket/PaintBucket.mesh.xml", 30.796974f, 8.417032f, 2.2264981f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //printy parking lot
        randomPlacements[12] = new Placement("Paint Can Item 12", "Models/objects/paintBucket/PaintBucket.mesh.xml", 68f, 8.5f, 7f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //lawlor parking lot
        randomPlacements[13] = new Placement("Paint Can Item 13", "Models/objects/paintBucket/PaintBucket.mesh.xml", 68.617f, 8.417032f, -31.746943f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //willets parking lot
        randomPlacements[14] = new Placement("Paint Can Item 14", "Models/objects/paintBucket/PaintBucket.mesh.xml", 128.51616f, 8.417032f, 189.80736f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //street by founders
        randomPlacements[15] = new Placement("Paint Can Item 15", "Models/objects/paintBucket/PaintBucket.mesh.xml", 1.850489f, 9.2f, 204.59644f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //ens parking lot 
        randomPlacements[16] = new Placement("Paint Can Item 16", "Models/objects/paintBucket/PaintBucket.mesh.xml", 90.30533f, 9.0f, 125.77196f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //behind apple
        randomPlacements[17] = new Placement("Paint Can Item 17", "Models/objects/paintBucket/PaintBucket.mesh.xml", -42.005356f, 8.417032f, 15.548188f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //doors to callan 
        randomPlacements[18] = new Placement("Paint Can Item 18", "Models/objects/paintBucket/PaintBucket.mesh.xml", 84.71723f, 8.417032f, 5.303521f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //behind lawlor 
        randomPlacements[19] = new Placement("Paint Can Item 19", "Models/objects/paintBucket/PaintBucket.mesh.xml", 5.7015142f, 8.417032f, 39.15853f, 1.5f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f); //behind the dmc
        
        int numCans = settingsMan.getPaintCansPerRound();
        
        if(numCans > randomPlacements.length) {
            System.err.println("The maximum number of paint cans allowed per round has been exceeded.  "
                    + "The maximum of " + randomPlacements.length + " cans will be placed.");
            numCans = randomPlacements.length;
        }
        
        for(int i = 0; i < numCans; i++) {
            Random random = new Random();
            int randomIndex = random.nextInt(randomPlacements.length);
            
            while(chosenPlacements.contains(randomPlacements[randomIndex])) {
                randomIndex = random.nextInt(randomPlacements.length);
            }
            
            chosenPlacements.add(randomPlacements[randomIndex]); 
        }
        return chosenPlacements;
    }    

    /** 
     * creates a rock Destination-item placement 
     */
    public ArrayList<Placement> createRockPlacement() {
        ArrayList<Placement> rockPlacement = new ArrayList<>();
        rockPlacement.add(new Placement("Rock Item", "Models/objects/rock/rock.obj", -22.0f, 7.3f, 93.0f, 2.7f, 0.0000f, 0.8499f, 0.0000f, 0.5270f, 0f, 0f, 0f, 0f));
        
        return rockPlacement;
    }    

    /** 
     * creates a random mascot Destination-item placement 
     */
    public ArrayList<Placement> createMascotPlacement() {
        Placement randomPlacements[] = new Placement[3];
        ArrayList<Placement> mascotPlacement = new ArrayList<>();       
        randomPlacements[0] = new Placement("Yellow Jacket Item 1", "Models/people/jacket/jacket.mesh.xml", -40.5f, 8.2f, -40f, 1.5f, 0f, 4f, 0f, 0f, 0f, 0f, 0f, 0f); // water tower
        randomPlacements[1] = new Placement("Yellow Jacket Item 2", "Models/people/jacket/jacket.mesh.xml", 91.8f, 9.75f, 192.3f, 1.5f, 0f, -1.5f, 0f, 1f, 0f, 0f, 0f, 0f); // founders
        randomPlacements[2] = new Placement("Yellow Jacket Item 3", "Models/people/jacket/jacket.mesh.xml", 41.49f, 8.65f, 158.3f, 1.5f, 0f, -0.45f, 0f, 1f, 0f, 0f, 0f, 0f); //milner
                                                                               
        Random random = new Random();
        int randomPlacementIndex = random.nextInt(randomPlacements.length);
        mascotPlacement.add(randomPlacements[randomPlacementIndex]);

        return mascotPlacement;
    }    

    /** 
     * creates a random refill Destination-item placement 
     */
    public ArrayList<Placement> createRefillStationPlacement() {
        Placement randomPlacements[] = new Placement[3];
        ArrayList<Placement> refillStationPlacement = new ArrayList<>();
        
        randomPlacements[0] = new Placement("SSC Building Item", "Models/buildings/ssc/ssc.mesh.xml", -55f, 5.7f, 100f, 1.75f, 0f, 0.7726f, 0f, 0.6349f, 0f, 0f, 0f, 0f);
        randomPlacements[1] = new Placement("ENS Building Item", "Models/buildings/ens/ens.mesh.xml", 4f, 6.5f, 172f, 3.95f, 0f, 0.0799f, 0f, 0.9968f, 0f, 0f, 0f, 0f);
        randomPlacements[2] = new Placement("Callan Building Item", "Models/buildings/callan/callan.mesh.xml", -36f, 7.0f, 5f, 23f, 0f, -0.0853f, 0f, 0.9964f, 0f, 0f, 0f, 0f);
       
        Random random = new Random();
        int randomPlacementIndex = random.nextInt(randomPlacements.length);
        refillStationPlacement.add(randomPlacements[randomPlacementIndex]);
        
        return refillStationPlacement;
    }    
    
    /** 
     * creates a paper pickup placement 
     */
    public ArrayList<Placement> createPaperPickupPlacement() {
        ArrayList<Placement> paperPlacement = new ArrayList<>();
        paperPlacement.add(new Placement("Tyler Building Item", "Models/buildings/tyler/tyler.obj", 53.6f, 7.9f, 195.5f, 3.4f, 0f, 0.7694f, 0f, 0.6388f, 0f, 0f, 0f, 0f));
        
        return paperPlacement;
    }       

    public void removePlacement(Placement placement) {
        Spatial spatial = rootNode.getChild(placement.getName());
        if (spatial != null) {
            spatial.removeFromParent();
        }
    }
    
    // removes all the objects from the placement array from the game world
    public void removePlacements(ArrayList<Placement> placements) {
        for (Placement placement : placements) {
            Spatial spatial = rootNode.getChild(placement.getName());
            if (spatial != null) {
                spatial.removeFromParent();
            }
        }
    } 
}
