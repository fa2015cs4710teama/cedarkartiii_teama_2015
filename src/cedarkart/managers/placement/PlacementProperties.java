package cedarkart.managers.placement;

import com.jme3.math.Vector3f;

/**
 *
 * @author Grant Dennison
 */
public class PlacementProperties {
    private String name;
    private String filePath;
    private float mass;
    private Vector3f hitbox;

    public PlacementProperties(String name, String filePath, float mass, float hitboxX,
            float hitboxY, float hitboxZ){
        this(name, filePath, mass, new Vector3f(hitboxX, hitboxY, hitboxZ));
    }

    public PlacementProperties(String name, String filePath, float mass, Vector3f hitbox){
        this.name = name;
        this.filePath = filePath;
        this.mass = mass;
        this.hitbox = hitbox;
    }
    
    public PlacementProperties(PlacementProperties template) {
        this(template.name, template.filePath, template.mass, template.hitbox);
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getFilePath() {
        return this.filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public float getMass() {
        return this.mass;
    }

    public void setMass(float mass) {
        this.mass = mass;
    }
    
    public Vector3f getHitbox() {
        return this.hitbox;
    }

    public void setHitbox(Vector3f hitbox) {
        this.hitbox = hitbox;
    }
}