package cedarkart.managers.placement;

import com.jme3.math.Vector3f;
import java.util.ArrayList;

/**
 *
 * @author Grant Dennison
 */
public class VehiclePlacementManager {
    private static final float CAR_MASS = 1f;
    private static final Vector3f CAR_HITBOX = new Vector3f(0.45f, 0.40f, 1.26f);
    
    private static final float VAN_MASS = 1f;
    private static final Vector3f VAN_HITBOX = new Vector3f(0.48f, 0.42f, 1.21f);
    
    private static final float[][] RED_CAR_TRANSFORMS = {
        {93.00098f, 0.40f, 121.599915f, 0.3f, 0.0f, 0.081282794f, 0.0f, 0.99669105f},
        {71.64967f, 0.40f, 101.89871f, 0.3f, 0.0f, -0.6319599f, 0.0f, 0.7750011f},
        {46.750023f, 0.40f, -76.65009f, 0.3f, 0.0f, 0.77987f, 0.0f, 0.62594163f},
        {48.89999f, 0.40f, -66.29946f, 0.3f, 0.0f, 0.77987f, 0.0f, 0.62594163f},
        {22.250397f, 0.40f, -71.19976f, 0.3f, 0.0f, -0.61958385f, 0.0f, 0.7849305f},
        {-23.549479f, 0.40f, -40.249683f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {32.700237f, 0.40f, -47.299576f, 0.3f, 0.0f, 0.7825137f, 0.0f, 0.6226336f},
        {30.000278f, 0.40f, -31.699812f, 0.3f, 0.0f, -0.6232441f, 0.0f, 0.78202736f},
        {55.199894f, 0.40f, -42.149654f, 0.3f, 0.0f, 0.9946239f, 0.0f, -0.10355363f},
        {56.74987f, 0.40f, -33.599785f, 0.3f, 0.0f, 0.08787441f, 0.0f, 0.99613154f},
        {64.59979f, 0.40f, -22.249956f, 0.3f, 0.0f, 0.089309126f, 0.0f, 0.996004f},
        {35.600193f, 0.40f, -5.500012f, 0.3f, 0.0f, 0.9953092f, 0.0f, -0.09674505f},
        {37.450165f, 0.40f, 8.550006f, 0.3f, 0.0f, 0.088500366f, 0.0f, 0.99607617f},
        {-4.7495513f, 0.40f, -54.79946f, 0.3f, 0.0f, 0.9933848f, 0.0f, -0.11483362f},
        {1.0004492f, 0.40f, -55.999443f, 0.3f, 0.0f, 0.088500366f, 0.0f, 0.99607617f},
        {-21.649508f, 0.40f, -31.099823f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {-19.049547f, 0.40f, -33.19979f, 0.3f, 0.0f, -0.68375653f, 0.0f, 0.7297102f},
        {84.50046f, 0.40f, 90.74803f, 0.3f, 0.0f, -0.6406502f, 0.0f, 0.7678329f},
        {77.45003f, 0.40f, 94.64827f, 0.3f, 0.0f, 0.7657362f, 0.0f, 0.64315486f},
        {-24.699463f, 0.40f, 59.849365f, 0.3f, 0.0f, -0.66401756f, 0.0f, 0.747717f},
        {-55.548996f, 0.40f, 43.69961f, 0.3f, 0.0f, 0.9997784f, 0.0f, -0.021052048f},
        {-17.04958f, 0.40f, 68.699585f, 0.3f, 0.0f, 0.74199444f, 0.0f, 0.67040604f},
        {-57.348965f, 0.40f, 55.749428f, 0.3f, 0.0f, 0.9995413f, 0.0f, -0.030285282f},
        {-30.69937f, 0.40f, -40.149685f, 0.3f, 0.0f, -0.6498111f, 0.0f, 0.7600958f},
        {22.899916f, 0.40f, 153.44939f, 0.3f, 0.0f, 0.77807915f, 0.0f, 0.6281663f},
        {-11.499577f, 0.40f, -30.549831f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {-67.44907f, 0.40f, 51.59949f, 0.3f, 0.0f, 0.007805344f, 0.0f, 0.99996954f},
        {-26.549433f, 0.40f, -25.099915f, 0.3f, 0.0f, -0.6427529f, 0.0f, 0.7660736f},
        {70.14958f, 0.40f, 93.248184f, 0.3f, 0.0f, -0.6322133f, 0.0f, 0.77479434f},        
    }; 
    private static final float[][] YELLOW_CAR_TRANSFORMS = {
        {95.35112f, 0.40f, 121.14989f, 0.3f, 0.0f, 0.081282794f, 0.0f, 0.99669105f},
        {16.10002f, 0.40f, 156.9996f, 0.3f, 0.0f, -0.6069951f, 0.0f, 0.7947055f},
        {8.249991f, 0.40f, 211.1029f, 0.3f, 0.0f, 0.78401124f, 0.0f, 0.62074673f},
        {-25.199455f, 0.40f, 55.59943f, 0.3f, 0.0f, -0.66401756f, 0.0f, 0.747717f},
        {25.749872f, 0.40f, 166.45018f, 0.3f, 0.0f, 0.77807915f, 0.0f, 0.6281663f},
        {77.900055f, 0.40f, 96.29837f, 0.3f, 0.0f, -0.6325408f, 0.0f, 0.774527f},
        {-17.04958f, 0.40f, 71.74977f, 0.3f, 0.0f, 0.74199444f, 0.0f, 0.67040604f},
        {-69.39919f, 0.40f, 56.399418f, 0.3f, 0.0f, 0.9995413f, 0.0f, -0.030285282f},
        {-56.198986f, 0.40f, 50.44951f, 0.3f, 0.0f, 0.007805344f, 0.0f, 0.99996954f},
        {-53.19903f, 0.40f, 38.54969f, 0.3f, 0.0f, 0.033375807f, 0.0f, 0.9994429f},
        {-29.249392f, 0.40f, -35.099762f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {-12.34958f, 0.40f, -33.599785f, 0.3f, 0.0f, -0.6425602f, 0.0f, 0.766235f},
        {-23.949472f, 0.40f, -41.84966f, 0.3f, 0.0f, -0.6427529f, 0.0f, 0.7660736f},
        {-17.799566f, 0.40f, -28.949856f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {1.5504487f, 0.40f, -50.19953f, 0.3f, 0.0f, 0.11171105f, 0.0f, 0.99374074f},
        {-14.599588f, 0.40f, -43.999626f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {54.6999f, 0.40f, -20.599981f, 0.3f, 0.0f, 0.089309126f, 0.0f, 0.996004f},
        {58.84984f, 0.40f, -28.59986f, 0.3f, 0.0f, 0.99545777f, 0.0f, -0.09520385f},
        {31.050262f, 0.40f, -28.349863f, 0.3f, 0.0f, -0.6232441f, 0.0f, 0.78202736f},
        {33.30023f, 0.40f, -44.149624f, 0.3f, 0.0f, 0.7825137f, 0.0f, 0.6226336f},
        {47.400013f, 0.40f, -73.94993f, 0.3f, 0.0f, 0.77987f, 0.0f, 0.62594163f},
        {23.000385f, 0.40f, -67.499535f, 0.3f, 0.0f, -0.61958385f, 0.0f, 0.7849305f},
        {69.29953f, 0.40f, 90.147995f, 0.3f, 0.0f, -0.6322133f, 0.0f, 0.77479434f},
    }; 
    private static final float[][] BLUE_CAR_TRANSFORMS = {
        {90.90085f, 0.40f, 121.99994f, 0.3f, 0.0f, 0.081282794f, 0.0f, 0.99669105f},
        {85.2505f, 0.40f, 94.748276f, 0.3f, 0.0f, 0.758697f, 0.0f, 0.65144366f},
        {22.249926f, 0.40f, 150.99924f, 0.3f, 0.0f, 0.77807915f, 0.0f, 0.6281663f},
        {68.64949f, 0.40f, 85.84773f, 0.3f, 0.0f, -0.6322133f, 0.0f, 0.77479434f},
        {69.64955f, 0.40f, 91.8481f, 0.3f, 0.0f, -0.6322133f, 0.0f, 0.77479434f},
        {15.500019f, 0.40f, 154.59946f, 0.3f, 0.0f, -0.6069951f, 0.0f, 0.7947055f},
        {15.000017f, 0.40f, 202.60239f, 0.3f, 0.0f, 0.78401124f, 0.0f, 0.62074673f},
        {5.3499804f, 0.40f, 201.90234f, 0.3f, 0.0f, 0.78401124f, 0.0f, 0.62074673f},
        {-23.499481f, 0.40f, 72.84984f, 0.3f, 0.0f, -0.66401756f, 0.0f, 0.747717f},
        {-54.44901f, 0.40f, 55.199436f, 0.3f, 0.0f, 0.9995413f, 0.0f, -0.030285282f},
        {-65.59896f, 0.40f, 56.19942f, 0.3f, 0.0f, 0.9995413f, 0.0f, -0.030285282f},
        {-64.39889f, 0.40f, 51.449493f, 0.3f, 0.0f, 0.007805344f, 0.0f, 0.99996954f},
        {-53.799023f, 0.40f, 50.44951f, 0.3f, 0.0f, 0.007805344f, 0.0f, 0.99996954f},
        {-19.149546f, 0.40f, -22.899948f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {-28.549402f, 0.40f, -32.899796f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {-13.299583f, 0.40f, -37.999718f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {-9.6495695f, 0.40f, -23.199944f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {-20.84952f, 0.40f, -41.04967f, 0.3f, 0.0f, -0.6239391f, 0.0f, 0.78147304f},
        {-18.599554f, 0.40f, -30.849827f, 0.3f, 0.0f, -0.6391657f, 0.0f, 0.76906914f},
        {7.100458f, 0.40f, -57.249424f, 0.3f, 0.0f, 0.088500366f, 0.0f, 0.99607617f},
        {-0.349551f, 0.40f, -49.799538f, 0.3f, 0.0f, 0.088500366f, 0.0f, 0.99607617f},
        {34.200214f, 0.40f, 9.350009f, 0.3f, 0.0f, 0.088500366f, 0.0f, 0.99607617f},
        {33.15023f, 0.40f, -5.250011f, 0.3f, 0.0f, 0.9953092f, 0.0f, -0.09674505f},
        {66.54991f, 0.40f, -22.64995f, 0.3f, 0.0f, 0.089309126f, 0.0f, 0.996004f},
        {62.249786f, 0.40f, -34.949764f, 0.3f, 0.0f, 0.08787441f, 0.0f, 0.99613154f},
        {28.900295f, 0.40f, -36.699738f, 0.3f, 0.0f, -0.6232441f, 0.0f, 0.78202736f},
        {35.500195f, 0.40f, -33.099792f, 0.3f, 0.0f, 0.7825137f, 0.0f, 0.6226336f},
        {48.399998f, 0.40f, -68.799614f, 0.3f, 0.0f, 0.77987f, 0.0f, 0.62594163f},
        {23.550377f, 0.40f, -65.49941f, 0.3f, 0.0f, -0.61958385f, 0.0f, 0.7849305f},
        {22.0004f, 0.40f, -73.09988f, 0.3f, 0.0f, -0.61958385f, 0.0f, 0.7849305f},
    }; 
    private static final float[][] SILVER_CAR_TRANSFORMS = {
        {97.75127f, 0.40f, 120.59985f, 0.3f, 0.0f, 0.081282794f, 0.0f, 0.99669105f},
        {84.800476f, 0.40f, 92.74815f, 0.3f, 0.0f, -0.6406502f, 0.0f, 0.7678329f},
        {14.250014f, 0.40f, 200.20224f, 0.3f, 0.0f, 0.78401124f, 0.0f, 0.62074673f},
        {14.450015f, 0.40f, 150.99924f, 0.3f, 0.0f, -0.6069951f, 0.0f, 0.7947055f},
        {70.54961f, 0.40f, 94.84828f, 0.3f, 0.0f, 0.7660074f, 0.0f, 0.6428317f},
        {16.350016f, 0.40f, 208.70276f, 0.3f, 0.0f, 0.78401124f, 0.0f, 0.62074673f},
        {24.59989f, 0.40f, 160.84984f, 0.3f, 0.0f, 0.77807915f, 0.0f, 0.6281663f},
        {10.049998f, 0.40f, 208.80276f, 0.3f, 0.0f, -0.6082859f, 0.0f, 0.793718f},
        {8.7999935f, 0.40f, 203.20242f, 0.3f, 0.0f, -0.6082859f, 0.0f, 0.793718f},
        {-56.198982f, 0.40f, 55.199436f, 0.3f, 0.0f, 0.9995413f, 0.0f, -0.030285282f},
        {-23.499481f, 0.40f, 75.14998f, 0.3f, 0.0f, -0.66401756f, 0.0f, 0.747717f},
        {70.4996f, 0.40f, 96.198364f, 0.3f, 0.0f, -0.6319599f, 0.0f, 0.7750011f},
        {-17.04958f, 0.40f, 67.14949f, 0.3f, 0.0f, 0.74199444f, 0.0f, 0.67040604f},
        {77.20001f, 0.40f, 92.598145f, 0.3f, 0.0f, -0.6322133f, 0.0f, 0.77479434f},
        {-59.798927f, 0.40f, 55.949425f, 0.3f, 0.0f, 0.9995413f, 0.0f, -0.030285282f},
        {-69.39919f, 0.40f, 52.199482f, 0.3f, 0.0f, 0.007805344f, 0.0f, 0.99996954f},
        {-59.048943f, 0.40f, 50.44951f, 0.3f, 0.0f, 0.007805344f, 0.0f, 0.99996954f},
        {-59.998928f, 0.40f, 39.499676f, 0.3f, 0.0f, 0.033375807f, 0.0f, 0.9994429f},
        {-16.84958f, 0.40f, -24.899918f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {-30.199377f, 0.40f, -38.249714f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {-28.349405f, 0.40f, -31.199821f, 0.3f, 0.0f, -0.6299521f, 0.0f, 0.776634f},
        {-14.299587f, 0.40f, -42.199654f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {-11.899578f, 0.40f, -32.249805f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {-21.149515f, 0.40f, -42.39965f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {-22.999487f, 0.40f, -38.649708f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {-22.0995f, 0.40f, -34.199776f, 0.3f, 0.0f, 0.7700068f, 0.0f, 0.6380357f},
        {9.000465f, 0.40f, -57.849415f, 0.3f, 0.0f, 0.088500366f, 0.0f, 0.99607617f},
        {7.050458f, 0.40f, -51.49951f, 0.3f, 0.0f, 0.9948778f, 0.0f, -0.10108441f},
        {4.450448f, 0.40f, -56.74943f, 0.3f, 0.0f, 0.9995333f, 0.0f, -0.030547919f},
        {-7.6495624f, 0.40f, -47.949566f, 0.3f, 0.0f, 0.99277574f, 0.0f, -0.11998499f},
        {31.550255f, 0.40f, 9.900011f, 0.3f, 0.0f, 0.088500366f, 0.0f, 0.99607617f},
        {24.65036f, 0.40f, -3.2000067f, 0.3f, 0.0f, 0.9953092f, 0.0f, -0.09674505f},
        {57.699856f, 0.40f, -21.049974f, 0.3f, 0.0f, 0.089309126f, 0.0f, 0.996004f},
        {61.849792f, 0.40f, -29.79984f, 0.3f, 0.0f, 0.99545777f, 0.0f, -0.09520385f},
        {61.849792f, 0.40f, -43.249638f, 0.3f, 0.0f, 0.9946239f, 0.0f, -0.10355363f},
        {50.699963f, 0.40f, -41.69966f, 0.3f, 0.0f, 0.9946239f, 0.0f, -0.10355363f},
        {27.90031f, 0.40f, -41.399666f, 0.3f, 0.0f, -0.6232441f, 0.0f, 0.78202736f},
        {36.650177f, 0.40f, -27.549877f, 0.3f, 0.0f, 0.7825137f, 0.0f, 0.6226336f},
        {24.150368f, 0.40f, -62.749344f, 0.3f, 0.0f, -0.61958385f, 0.0f, 0.7849305f},
    }; 

    private static final float[][] RED_VAN_TRANSFORMS = {
        {34.750206f, 0.42f, -36.899734f, 0.3f, 0.0f, 0.7825137f, 0.0f, 0.6226336f},
        {53.799915f, 0.42f, -42.149654f, 0.3f, 0.0f, 0.9946239f, 0.0f, -0.10355363f},
        {27.25032f, 0.42f, -3.6500063f, 0.3f, 0.0f, 0.9953092f, 0.0f, -0.09674505f},
        {-61.19891f, 0.42f, 51.449493f, 0.3f, 0.0f, 0.007805344f, 0.0f, 0.99996954f},
        {6.599985f, 0.42f, 206.65263f, 0.3f, 0.0f, 0.78401124f, 0.0f, 0.62074673f},
        {5.799982f, 0.42f, 204.5525f, 0.3f, 0.0f, 0.78401124f, 0.0f, 0.62074673f},
    }; 
    private static final float[][] YELLOW_VAN_TRANSFORMS = {
        {-24.499466f, 0.42f, 62.999317f, 0.3f, 0.0f, -0.66401756f, 0.0f, 0.747717f},
        {6.849986f, 0.42f, 208.30273f, 0.3f, 0.0f, 0.78401124f, 0.0f, 0.62074673f},
        {-53.799023f, 0.42f, 43.69961f, 0.3f, 0.0f, 0.9997784f, 0.0f, -0.021052048f},
        {59.149834f, 0.42f, -34.349773f, 0.3f, 0.0f, 0.08787441f, 0.0f, 0.99613154f},
        {15.000017f, 0.42f, 152.54933f, 0.3f, 0.0f, 0.78401124f, 0.0f, 0.62074673f},
        {27.400318f, 0.42f, -44.44962f, 0.3f, 0.0f, -0.6232441f, 0.0f, 0.78202736f},
    }; 
    private static final float[][] BLUE_VAN_TRANSFORMS = {
        {25.149881f, 0.42f, 164.40005f, 0.3f, 0.0f, 0.77807915f, 0.0f, 0.6281663f},
        {-62.748882f, 0.42f, 56.19942f, 0.3f, 0.0f, 0.9995413f, 0.0f, -0.030285282f},
        {-17.04958f, 0.42f, 70.04967f, 0.3f, 0.0f, 0.74199444f, 0.0f, 0.67040604f},
        {27.25032f, 0.42f, 10.300013f, 0.3f, 0.0f, 0.088500366f, 0.0f, 0.99607617f},
        {-66.199f, 0.42f, 43.69961f, 0.3f, 0.0f, 0.9997784f, 0.0f, -0.021052048f},
        {58.549843f, 0.42f, -42.749645f, 0.3f, 0.0f, 0.9946239f, 0.0f, -0.10355363f},
        {63.849762f, 0.42f, -30.249834f, 0.3f, 0.0f, 0.99545777f, 0.0f, -0.09520385f},
        {31.700253f, 0.42f, -50.099533f, 0.3f, 0.0f, 0.7825137f, 0.0f, 0.6226336f},
    }; 
    private static final float[][] SILVER_VAN_TRANSFORMS = {
        {15.70002f, 0.42f, 205.40256f, 0.3f, 0.0f, 0.78401124f, 0.0f, 0.62074673f},
        {8.149991f, 0.42f, 201.00229f, 0.3f, 0.0f, -0.6082859f, 0.0f, 0.793718f},
        {29.350288f, 0.42f, -34.299774f, 0.3f, 0.0f, -0.6232441f, 0.0f, 0.78202736f},
        {36.200184f, 0.42f, -29.899841f, 0.3f, 0.0f, 0.7825137f, 0.0f, 0.6226336f},
        {23.399908f, 0.42f, 155.09949f, 0.3f, 0.0f, 0.77807915f, 0.0f, 0.6281663f},
    }; 
    
    static ArrayList<Placement> createVehiclePlacements() {
        PlacementProperties carProperties = new PlacementProperties("Car", 
                "Models/vehicles/car_color/car.j3o", CAR_MASS, 
                CAR_HITBOX.x, CAR_HITBOX.y, CAR_HITBOX.z);
        
        PlacementProperties redCarProperties = new PlacementProperties(carProperties);
        redCarProperties.setFilePath("Models/vehicles/car_red/car.j3o");

        PlacementProperties yellowCarProperties = new PlacementProperties(carProperties);
        yellowCarProperties.setFilePath("Models/vehicles/car_yellow/car.j3o");

        PlacementProperties blueCarProperties = new PlacementProperties(carProperties);
        blueCarProperties.setFilePath("Models/vehicles/car_blue/car.j3o");

        PlacementProperties silverCarProperties = new PlacementProperties(carProperties);
        silverCarProperties.setFilePath("Models/vehicles/car_silver/car.j3o");

        
        PlacementProperties vanProperties = new PlacementProperties("Van",
                "Models/vehicles/van_color/van.j3o", VAN_MASS, 
                VAN_HITBOX.x, VAN_HITBOX.y, VAN_HITBOX.z);
        
        PlacementProperties redVanProperties = new PlacementProperties(vanProperties);
        redVanProperties.setFilePath("Models/vehicles/van_red/van.j3o");

        PlacementProperties yellowVanProperties = new PlacementProperties(vanProperties);
        yellowVanProperties.setFilePath("Models/vehicles/van_yellow/van.j3o");

        PlacementProperties blueVanProperties = new PlacementProperties(vanProperties);
        blueVanProperties.setFilePath("Models/vehicles/van_blue/van.j3o");

        PlacementProperties silverVanProperties = new PlacementProperties(vanProperties);
        silverVanProperties.setFilePath("Models/vehicles/van_silver/van.j3o");

        ArrayList<Placement> redCarPlacements = PlacementManager.placementArrayList(redCarProperties, RED_CAR_TRANSFORMS);
        ArrayList<Placement> yellowCarPlacements = PlacementManager.placementArrayList(yellowCarProperties, YELLOW_CAR_TRANSFORMS);
        ArrayList<Placement> blueCarPlacements = PlacementManager.placementArrayList(blueCarProperties, BLUE_CAR_TRANSFORMS);
        ArrayList<Placement> silverCarPlacements = PlacementManager.placementArrayList(silverCarProperties, SILVER_CAR_TRANSFORMS);

        ArrayList<Placement> redVanPlacements = PlacementManager.placementArrayList(redVanProperties, RED_VAN_TRANSFORMS);
        ArrayList<Placement> yellowVanPlacements = PlacementManager.placementArrayList(yellowVanProperties, YELLOW_VAN_TRANSFORMS);
        ArrayList<Placement> blueVanPlacements = PlacementManager.placementArrayList(blueVanProperties, BLUE_VAN_TRANSFORMS);
        ArrayList<Placement> silverVanPlacements = PlacementManager.placementArrayList(silverVanProperties, SILVER_VAN_TRANSFORMS);
        
        ArrayList<Placement> vehiclePlacements = new ArrayList<>();;
        
        vehiclePlacements.addAll(redCarPlacements);
        vehiclePlacements.addAll(yellowCarPlacements);
        vehiclePlacements.addAll(blueCarPlacements);
        vehiclePlacements.addAll(silverCarPlacements);
        
        vehiclePlacements.addAll(redVanPlacements);
        vehiclePlacements.addAll(yellowVanPlacements);
        vehiclePlacements.addAll(blueVanPlacements);
        vehiclePlacements.addAll(silverVanPlacements);
        
        return vehiclePlacements;
    }
}
