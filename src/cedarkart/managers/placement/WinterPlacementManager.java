package cedarkart.managers.placement;

import java.util.ArrayList;

public class WinterPlacementManager {
    public static ArrayList<Placement> createWinterDetailPlacements() {
        ArrayList<Placement> placements = new ArrayList<>();

        Placement ice = new Placement("Ice", "Models/objects/ice/ice.mesh.xml", 10f, 6f, 90f, 1.25f, 0.0f, 0.0f, 0.0f, 0.0f, 0f, 0f, 0f, 0f);
        ice.fiction = 0.01f;
        placements.add(ice);

        return placements;
    }    
}
