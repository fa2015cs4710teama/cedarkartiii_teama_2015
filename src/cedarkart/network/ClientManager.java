
package cedarkart.network;

import cedarkart.Main;
import cedarkart.managers.GameManager;
import cedarkart.managers.GameManager.State;
import cedarkart.managers.Item;
import cedarkart.managers.placement.Placement;
import cedarkart.managers.PlayerManager;
import cedarkart.managers.Powerup;
import cedarkart.managers.SettingsManager.Weather;
import cedarkart.managers.WorldManager;
import cedarkart.managers.mode.GameModeManager.GameType;
import com.jme3.network.Client;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.network.Network;
import com.jme3.network.serializing.Serializer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 *
 * @author Paul Marshall
 */
public class ClientManager {
    
    private final Main app;
    private final GameManager gameMan;
    private final WorldManager worldMan;
    private final PlayerManager playerMan;
    
    private Client client;
    
    public ClientManager(Main app, GameManager gameMan, WorldManager worldMan,
            PlayerManager playerMan) {
        this.app = app;
        this.gameMan = gameMan;
        this.worldMan = worldMan;
        this.playerMan = playerMan;
        
        Serializer.registerClasses(ActorDestroyMessage.class, ActorInit.class, 
                ActorInitMessage.class, ActorUpdate.class,
                ActorUpdateMessage.class, GameStateMessage.class, 
                InitMessage.class, ItemMessage.class, 
                ModeMessage.class, ObjectiveMessage.class,
                PowerupMessage.class, ScoreMessage.class, TrophyMessage.class, 
                Item.class, Placement.class, Powerup.class);
        
        // Start client:
        try {
            String host = gameMan.getHost();
            
            if (host == null) {
                host = "localhost";
            }
            
            this.client = Network.connectToServer(host, gameMan.getPort());
            this.client.addMessageListener(new ClientListener(),
                    ActorDestroyMessage.class, ActorInit.class, 
                    ActorInitMessage.class, ActorUpdate.class,
                    ActorUpdateMessage.class, GameStateMessage.class, 
                    InitMessage.class, ItemMessage.class, 
                    ModeMessage.class, ObjectiveMessage.class,
                    PowerupMessage.class, ScoreMessage.class, TrophyMessage.class, 
                    Item.class, Placement.class, Powerup.class);
            this.client.start();
            this.client.send(init());
        } catch (IOException ex) {
            System.err.println("[CLIENT] Client could not connect to provided host.");
        }
    }
    
    private ActorInitMessage init() {
        ActorInit init = new ActorInit(this.client.getId(), this.gameMan
                .getSettingsMan().getVehicle().name());
        return new ActorInitMessage(init);
    }
    
    public int getId() {
        if (this.client != null) {
            return this.client.getId();
        } else {
            return -1;
        }
    }
    
    public void disconnect() {
        if (this.client != null) {
            this.client.close();
            this.client = null;
        }
    }
    
    public void update(ActorUpdate update) {
        if (this.client != null) {
            this.client.send(new ActorUpdateMessage(update));
        }
    }
    
    public void pause() {
        if (this.client != null) {
            this.client.send(new GameStateMessage(State.PAUSE.name()));
        }
    }
    
    public void resume() {
        if (this.client != null) {
            this.client.send(new GameStateMessage(State.RESUME.name()));
        }
    }
    
    public void end() {
        if (this.client != null) {
            this.client.send(new GameStateMessage(State.END.name()));
        }
    }
    
    public void itemsAddSend(ArrayList<Item> itemList) {
        if (this.client != null) {
            this.client.send(new ItemMessage(ItemMessage.Type.ADD, itemList));
            System.out.println("Sent items add");
        }
    }
    
    private void itemsAddReceive(final ArrayList<Item> itemList) {
        this.app.enqueue(new Callable<Object>() {
            @Override public Object call() throws Exception {
                gameMan.getTrophyMan().placeItems(itemList);
                return itemList;
            }
        });
    }
    
    public void itemRemoveSend(Item item) {
        if (this.client != null) {
            System.out.println("sending item remove request");
            this.client.send(new ItemMessage(ItemMessage.Type.REMOVE, item));
        }
    }
    
    private void itemRemoveReceive(final Item item) {
        this.app.enqueue(new Callable<Object>() {
            @Override public Object call() throws Exception {
                System.out.println("removing item per server request");
                gameMan.getTrophyMan().removeItem(item);
                return item;
            }
        });
    }
    
    public void itemsClearSend() {
        if (this.client != null) {
            this.client.send(new ItemMessage(ItemMessage.Type.CLEAR));
        }
    }
    
    private void itemsClearReceive() {
        this.app.enqueue(new Callable<Object>() {
            @Override public Object call() throws Exception {
                gameMan.getTrophyMan().clearItems();
                return null;
            }
        });
    }
    
    public void modeSend(GameType mode) {
        if (this.client != null) {
            this.client.send(new ModeMessage(mode));
        }
    }
    
    private void modeReceive(final GameType mode) {
        this.app.enqueue(new Callable<Object>() {
            @Override public Object call() throws Exception {
                gameMan.getGameModeMan().setActiveMode(mode);
                playerMan.resetCollections();
                return mode;
            }
        });
    }
    
    public void objectiveSend(String objective) {
        if (this.client != null) {
            this.client.send(new ObjectiveMessage(objective));
        }
    }
    
    private void objectiveReceive(final String objective) {
        this.app.enqueue(new Callable<Object>() {
            @Override public Object call() throws Exception {
                gameMan.getHUDMan().updateObjective(objective);
                return objective;
            }
        });
    }
    
    public void scoreSend(int score, boolean isFinal) {
        if (this.client != null) {
            this.client.send(new ScoreMessage(score, playerMan.getPlayerId(), isFinal));
        }
    }
    
    private void scoreReceive(final int score, final int playerId) {
        this.app.enqueue(new Callable<Object>() {
            @Override public Object call() throws Exception {
                gameMan.getHUDMan().updateScore(playerId, score);
                return score;
            }
        });
    }
    
    public void capture() {
        if (this.client != null) {
            this.client.send(new TrophyMessage(getId()));
        }
    }
    
    public void powerup(Powerup powerup) {
        if (this.client != null) {
            this.client.send(new PowerupMessage(powerup));
        }
    }
    
    private void updateActor(final ActorUpdate update) {
        this.app.enqueue(new Callable<ActorUpdate>() {
            @Override public ActorUpdate call() throws Exception {
                worldMan.updatePlayer(update);
                return update;
            }
        });
    }
    
    private void applyPowerup(final Powerup powerup) {
        this.app.enqueue(new Callable<Powerup>() {
            @Override public Powerup call() throws Exception {
                gameMan.getTrophyMan().capturePowerup(powerup.getId());
                if (powerup.getTarget() < 0 || powerup.getTarget() == getId()) {
                    switch (powerup.getType()) {
                        case BEES:
                            playerMan.setBees(true);
                            break;
                        case IMPULSE:
                            playerMan.impulsePowerup();
                            break;
                        case GRAVITY:
                            playerMan.setGravity(false);
                            break;
                        case RANDOMIZER:
                            playerMan.randomizeLocation();
                            break;
                    }
                }
                
                return powerup;
            }
        });
    }
    
    private void add(final ActorInit init) {
        this.app.enqueue(new Callable<ActorInit>() {
            @Override public ActorInit call() throws Exception {
                worldMan.addPlayer(init);
                return init;
            }
        });
    }
    
    private void remove(final int id) {
        this.app.enqueue(new Callable<Integer>() {
            @Override public Integer call() throws Exception {
                worldMan.removePlayer(id);
                return id;
            }
        });
    }
    
    private void starter() {
        this.app.enqueue(new Callable() {
            @Override public Object call() throws Exception {
                gameMan.setRunning(true);
                return null;
            }
        });
    }
    
    private void pauser() {
        this.app.enqueue(new Callable() {
            @Override public Object call() throws Exception {
                gameMan.pauseGame(false);
                return null;
            }
        });
    }
    
    private void resumer() {
        this.app.enqueue(new Callable() {
            @Override public Object call() throws Exception {
                gameMan.resumeGame();
                return null;
            }
        });
    }
    
    private void ender(final String endGameMessage) {
        this.app.enqueue(new Callable() {
            @Override public Object call() throws Exception {
                disconnect();
                gameMan.endGame(endGameMessage);
                return null;
            }
        });
    }
    
    private void initer(final int seed, final int playerId, final GameType selectedMode, final Weather weather) {
        this.app.enqueue(new Callable<Integer>() {
            @Override public Integer call() throws Exception {
                gameMan.getTrophyMan().setSeed(seed);
                gameMan.getPlayerMan().setPlayerId(playerId);
                gameMan.getSettingsMan().setGameMode(selectedMode);
                if (weather != null) {
                    gameMan.getSettingsMan().setWeather(weather);
                }
                gameMan.startGame();
                return seed;
            }
        });
    }
    
    private class ClientListener implements MessageListener<Client> {
        @Override public void messageReceived(Client source, Message m) {
            if (m instanceof ActorUpdateMessage) {
                updateActor(((ActorUpdateMessage) m).getContent());
            } else if (m instanceof PowerupMessage) {
                applyPowerup(((PowerupMessage) m).getPowerup());
                System.out.println("[CLIENT] Powerup message received");
            } else if (m instanceof ActorInitMessage) {
                add(((ActorInitMessage) m).getInit());
            } else if (m instanceof ActorDestroyMessage) {
                remove(((ActorDestroyMessage) m).getID());
            } else if (m instanceof GameStateMessage) {
                GameStateMessage message = (GameStateMessage) m;
                State state = State.valueOf(message.getState());
                switch (state) {
                    case START:
                        starter();
                        break;
                    case PAUSE:
                        pauser();
                        break;
                    case RESUME:
                        resumer();
                        break;
                    case END:
                        ender(message.getData());
                        break;
                }
            } else if (m instanceof InitMessage) {
                InitMessage message = (InitMessage) m;
                initer(message.getSeed(), message.getPlayerId(),
                        message.getSelectedMode(), message.getWeather());
            } else if (m instanceof ItemMessage) {
                ItemMessage message = (ItemMessage) m;
                switch(message.getType()) {
                    case ADD:
                        itemsAddReceive(message.getItems());
                        break;
                    case REMOVE:
                        System.out.println("received item remove request");
                        itemRemoveReceive(message.getItems().get(0));
                        break;
                    case CLEAR:
                        itemsClearReceive();
                        break;
                }
            } else if (m instanceof ModeMessage) {
                modeReceive(((ModeMessage) m).getType());
            } else if (m instanceof ObjectiveMessage) {
                objectiveReceive(((ObjectiveMessage) m).getObjective());
            } else if (m instanceof ScoreMessage) {
                ScoreMessage message = (ScoreMessage) m;
                scoreReceive(message.getScore(), message.getPlayerId());
            }
        }
        
    }
}
