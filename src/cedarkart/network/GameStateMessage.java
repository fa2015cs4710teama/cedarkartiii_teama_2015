
package cedarkart.network;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author Paul Marshall
 */
@Serializable
public class GameStateMessage extends AbstractMessage {
    
    private String state;
    private String data;
    
    public GameStateMessage() {
        this(null, null);
    }
    
    public GameStateMessage(String state) {
        this(state, null);
    }
    
    public GameStateMessage(String state, String data) {
        this.state = state;
        this.data = data;
    }
    
    public String getData() {
        return data;
    }

    public String getState() {
        return state;
    }
}
