
package cedarkart.network;

import cedarkart.managers.mode.GameModeManager.GameType;
import cedarkart.managers.SettingsManager.Weather;
import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author Paul Marshall
 */
@Serializable
public class InitMessage extends AbstractMessage {
    
    private int seed;
    private int playerId;
    private String selectedMode;
    private String weather;
    
    public InitMessage() {
        this.seed = -1;
        this.weather = "SUNRISE";
    }
    
    public InitMessage(int seed, int playerId, GameType selectedMode, Weather weather) {
        this.seed = seed;
        this.playerId = playerId;
        this.selectedMode = selectedMode.name();
        this.weather = weather.name();
    }

    public int getSeed() {
        return seed;
    }

    public void setSeed(int seed) {
        this.seed = seed;
    }
    
    public int getPlayerId() {
        return playerId;
    }
    
    public GameType getSelectedMode() {
        return GameType.valueOf(selectedMode);
    }

    public Weather getWeather() {
        return Weather.valueOf(weather);
    }

    public void setWeather(Weather weather) {
        this.weather = weather.name();
    }
 }
