package cedarkart.network;

import cedarkart.managers.Item;
import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Grant Dennison
 */
@Serializable
public class ItemMessage extends AbstractMessage {
    
    private ArrayList<Item> items;
    private String type;
    
    public enum Type {
        CLEAR,
        ADD,
        REMOVE
    }
    
    public ItemMessage() {
        this.items = new ArrayList<>();
    }
    
    public ItemMessage(Type type) {
        this.items = new ArrayList<>();
        this.type = type.name();
    }
    
    public ItemMessage(Type type, ArrayList<Item> items) {
        this(type);
        this.items.addAll(items);
    }
    
    public ItemMessage(Type type, Item item) {
        this(type);
        this.items.add(item);
    }

    public ArrayList<Item> getItems() {
        return this.items;
    }

    public Type getType() {
        return Type.valueOf(type);
    }
}
