package cedarkart.network;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Grant Dennison
 */
public class MasterGameManager {
    
    public final int MAX_PLAYERS = 4;
    private Map<Integer, Integer> clientIdToPlayerId = new HashMap<>();
    private boolean[] playersInNetwork = new boolean[MAX_PLAYERS];
    private int playerCount = 0;
    private int[] playerScores = new int[MAX_PLAYERS];
    private int playerScoresSubmitted = 0;
    
    private Servlet owner;
    
    public MasterGameManager(Servlet owner) {
        this.owner = owner;
        
        for(int i = 0; i < MAX_PLAYERS; i++) {
            playersInNetwork[i] = false;
            playerScores[i] = -1;
        }
    }
    
    /**
     * Generate a game-over message and send it to all clients.
     */
    private void broadcastGameEnd() {
        int winningScore = -1;
        ArrayList<String> winners = new ArrayList<>();
        
        for(int i = 0; i < playerCount; i++) {
            if(playerScores[i] > winningScore) {
                winners.clear();
                winningScore = playerScores[i];
            }
            if(playerScores[i] >= winningScore) {
                winners.add("Player " + (i + 1));
            }
        }
        
        String endGameMessage = "";
        
        switch(winners.size()) {
            case 1: 
                endGameMessage = winners.get(0) + " wins!";
                break;
            case 2:
                endGameMessage = winners.get(0) + " and " + winners.get(1) + " tied for the win!";
                break;
            case 3:
                endGameMessage = winners.get(0) + ", " + winners.get(1) + ", and " + winners.get(2) + " tied for the win!";
                break;
            case 4:
                endGameMessage = "Everybody wins! (That's just another way of saying no one wins.)";
                break;
        }
        
        owner.endGame(endGameMessage);
    }
    
    /**
     * @param clientId the ID provided from the getId method of Servlet's connections
     * @throws NetworkException if clientId is already registered
     */
    private void checkIsClientIdUsed(int clientId) throws NetworkException {
        if(clientIdToPlayerId.containsKey(clientId)) {
            throw new NetworkException("Client ID is already taken");
        }
    }
    
    /**
     * @throws NetworkException if the game is already full
     */
    private void checkIsGameFull() throws NetworkException {
        if(isGameFull()) {
            throw new NetworkException("Game is at full capacity");
        }
    }
    
    /**
     * @param playerId
     * @throws NetworkException if the playerId is invalid
     */
    private void checkIsPlayerIdValid(int playerId) throws NetworkException {
        if(playerId > MAX_PLAYERS) {
            throw new NetworkException("Out-of-bounds playerId");
        }
        if(!playersInNetwork[playerId - 1]) {
            throw new NetworkException("playerId is not in use this game");
        }
    }
    
    /**
     * At the end of the game, this function should be called once by the Servlet for each player score.
     * Once all of the players' scores have been submitted, all players are sent an end-game message.
     * @param playerId
     * @param score the player's final score
     * @throws NetworkException if playerId is out of range or was not registered at the beginning of the session
     */
    public void submitFinalScore(int playerId, int score) throws NetworkException {
        checkIsPlayerIdValid(playerId);
        if(playerScores[playerId - 1] < 0) {
            playerScores[playerId - 1] = score;
            playerScoresSubmitted++;
        }
        
        if(playerScoresSubmitted == playerCount) {
            broadcastGameEnd();
        }
    }
    
    /**
     * Notify MasterGameMan of a joining player.
     * @param clientId the ID provided from the getId method of Servlet's connections
     * @return the playerId of the added player
     * @throws NetworkException if the game is full or the clientId has already been used
     */
    public int addPlayer(int clientId) throws NetworkException {
        checkIsGameFull();
        checkIsClientIdUsed(clientId);
        int i;
        for(i = 0; i < MAX_PLAYERS; i++) {
            if(!playersInNetwork[i]) {
                playersInNetwork[i] = true;
                break;
            }
        }
        playerCount++;
        clientIdToPlayerId.put(clientId, i);
        return i + 1;
    }
    
    /**
     * Notify MasterGameMan that a player has left the game.
     * @param clientId the ID provided from the getId method of Servlet's connections
     * @throws NetworkException if clientId was not registered at the beginning of the session
     */
    public void removePlayer(int clientId) throws NetworkException {
        if(!clientIdToPlayerId.containsKey(clientId)) {
            throw new NetworkException("Client ID is not registered with MasterGameMan");
        }
        int playerId = clientIdToPlayerId.get(clientId);
        playersInNetwork[playerId - 1] = false;
        clientIdToPlayerId.remove(clientId);
        playerCount--;
    }
    
    public int getMaxPlayers() {
        return MAX_PLAYERS;
    }
    
    public boolean isGameFull() {
        return playerCount >= MAX_PLAYERS;
    }
}
