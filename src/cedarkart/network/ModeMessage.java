package cedarkart.network;

import cedarkart.managers.mode.GameModeManager.GameType;
import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author Grant Dennison
 */
@Serializable
public class ModeMessage extends AbstractMessage {
    
    private String type;
    
    public ModeMessage() {
        type = null;
    }
    
    public ModeMessage(GameType type) {
        this.type = type.name();
    }
    
    public GameType getType() {
        return GameType.valueOf(type);
    }
}
