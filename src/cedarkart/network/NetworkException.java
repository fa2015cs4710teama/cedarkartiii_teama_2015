package cedarkart.network;

/**
 *
 * @author Grant Dennison
 */
public class NetworkException extends RuntimeException {
    public NetworkException(String err) {
        super(err);
    }
}
