package cedarkart.network;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author Grant Dennison
 */
@Serializable
public class ObjectiveMessage extends AbstractMessage {
    
    private String objective;
    
    public ObjectiveMessage() {
        this.objective = "";
    }
    
    public ObjectiveMessage(String objective) {
        this.objective = objective;
    }
    
    public String getObjective() {
        return this.objective;
    }
}
