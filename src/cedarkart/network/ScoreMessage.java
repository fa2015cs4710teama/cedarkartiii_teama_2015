package cedarkart.network;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

/**
 *
 * @author Grant Dennison
 */
@Serializable
public class ScoreMessage extends AbstractMessage {
    
    private int score;
    private int playerId;
    private boolean isFinal;
    
    public ScoreMessage() {
        this(0, 0, false);
    }
    
    public ScoreMessage(int score, int playerId, boolean isFinal) {
        this.score = score;
        this.playerId = playerId;
        this.isFinal = isFinal;
    }
    
    public int getScore() {
        return score;
    }
    
    public int getPlayerId() {
        return playerId;
    }
    
    public boolean getIsFinal() {
        return isFinal;
    }
}
