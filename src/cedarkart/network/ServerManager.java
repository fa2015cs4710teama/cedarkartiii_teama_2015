
package cedarkart.network;

import cedarkart.Main;
import cedarkart.managers.GameManager;
import cedarkart.managers.GameManager.State;
import cedarkart.managers.SettingsManager.Weather;
import cedarkart.managers.mode.GameModeManager.GameType;

/**
 *
 * @author Paul Marshall
 */
public class ServerManager {
    
    private final Main app;
    private final GameManager gameMan;
    
    private Servlet server = null;
    
    public ServerManager(Main app, GameManager gameMan) {
        this.app = app;
        this.gameMan = gameMan;
        
        this.server = new Servlet(this.gameMan.getPort());
    }
    
    public void start(GameType selectedMode, Weather map) {
        this.server.start(selectedMode, map);
    }
    
    public void stop() {
        this.server.stop();
    }
    
    public void setState(State state) {
        this.server.forceState(state);
    }
}
