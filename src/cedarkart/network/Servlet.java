
package cedarkart.network;

import cedarkart.managers.GameManager.State;
import cedarkart.managers.Item;
import cedarkart.managers.placement.Placement;
import cedarkart.managers.Powerup;
import cedarkart.managers.SettingsManager.Weather;
import cedarkart.managers.mode.GameModeManager.GameType;
import com.jme3.network.ConnectionListener;
import com.jme3.network.Filters;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;
import com.jme3.network.Network;
import com.jme3.network.Server;
import com.jme3.network.serializing.Serializer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paul Marshall
 */
public class Servlet {
    
    private static final Logger LOG =  Logger.getLogger(Servlet.class.getName());
    
    private int nextPlayerId = 1;
    private MasterGameManager masterGameMan;
    private int seed = (new Random()).nextInt();
    private Random random = new Random(seed);
    private Server server = null;
    private ArrayList<ActorInit> actors = new ArrayList<>();
    private GameType selectedMode;
    private Weather map;
    
    public Servlet(int port) {
        masterGameMan = new MasterGameManager(this);
        
        Serializer.registerClasses(ActorDestroyMessage.class, ActorInit.class, 
                ActorInitMessage.class, ActorUpdate.class,
                ActorUpdateMessage.class, GameStateMessage.class, 
                InitMessage.class, ItemMessage.class, 
                ModeMessage.class, ObjectiveMessage.class,
                PowerupMessage.class, ScoreMessage.class, TrophyMessage.class, 
                Item.class, Placement.class, Powerup.class);
        
        try {
            this.server = Network.createServer(port);
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, "Unable to create server.", ex);
        }
        
        if (this.server != null) {
            this.server.addMessageListener(new ServerListener(),
                    ActorDestroyMessage.class, ActorInit.class, 
                    ActorInitMessage.class, ActorUpdate.class,
                    ActorUpdateMessage.class, GameStateMessage.class, 
                    InitMessage.class, ItemMessage.class, 
                    ModeMessage.class, ObjectiveMessage.class,
                    PowerupMessage.class, ScoreMessage.class, TrophyMessage.class, 
                    Item.class, Placement.class, Powerup.class);
            this.server.addConnectionListener(new ClientListener());
        }
    }
    
    public void endGame(String endGameMessage) {
        server.broadcast(new GameStateMessage(State.END.name(), endGameMessage));
    }
    
    public void start(GameType selectedMode, Weather map) {
        this.selectedMode = selectedMode;
        this.map = map;
        if (this.server != null && !this.server.isRunning()) {
            this.server.start();
            System.out.println("[SERVER] Server started.");
        }
    }
    
    public void stop() {
        if (this.server != null && this.server.isRunning()) {
            this.server.close();
        }
    }
    
    public void forceState(State state) {
        server.broadcast(new GameStateMessage(state.name()));
    }
    
    private class ServerListener implements MessageListener<HostedConnection> {

        @Override
        public void messageReceived(HostedConnection source, Message m) {
//            if(!(m instanceof ActorUpdateMessage)) {
//                System.out.println("some message to broadcast");
//            }
            if (m instanceof ActorInitMessage) {
                ActorInitMessage message = (ActorInitMessage) m;
                if (message.getInit().getId() < 0) {
                    message.getInit().setId(source.getId());
                }
                server.broadcast(Filters.notEqualTo(source), message);
                for (ActorInit init: actors) {
                    source.send(new ActorInitMessage(init));
                }
                actors.add(message.getInit());
            } else if (m instanceof PowerupMessage) {
                PowerupMessage message = (PowerupMessage) m;
                System.out.println("[SERVER] Powerup message from " + source.getId());
                int target = message.getPowerup().getTarget();
                if (target < 0) {
                    server.broadcast(Filters.notEqualTo(source), message);
                } else {
                    server.getConnection(target).send(message);
                }
            } else if (m instanceof ScoreMessage) {
                ScoreMessage message = (ScoreMessage) m;
                if (message.getIsFinal()) {
                    masterGameMan.submitFinalScore(message.getPlayerId(), message.getScore());
                }
                server.broadcast(Filters.notEqualTo(source), message);
            } else if (m instanceof TrophyMessage) {
                TrophyMessage message = (TrophyMessage) m;
                message.setNext(random.nextInt());
                server.broadcast(Filters.notEqualTo(source), message);
                System.out.println("[SERVER] Trophy message from " + source.getId());
            } else { //Catch all other messages and broadcast them
                server.broadcast(Filters.notEqualTo(source), m);
                System.out.println("[SERVER] ? message from " + source.getId());
            }
        }
    }
    
    private void removeConnection(HostedConnection conn) {
        ActorDestroyMessage message = new ActorDestroyMessage(conn.getId());
        server.broadcast(message);
        for (ActorInit init : actors) {
            if (init.getId() == conn.getId()) {
                actors.remove(init);
                break;
            }
        }
    }
    
    private class ClientListener implements ConnectionListener {

        @Override
        public void connectionAdded(Server server, HostedConnection conn) {
            if(masterGameMan.isGameFull()) {
                conn.close(masterGameMan.getMaxPlayers() + " have already joined the game. Closing the connection...");
            }
            else {
                conn.send(new InitMessage(seed, masterGameMan.addPlayer(conn.getId()), selectedMode, map));
            }            
        }
        
        @Override
        public void connectionRemoved(Server server, HostedConnection conn) {
            masterGameMan.removePlayer(conn.getId());
            removeConnection(conn);
            if(server.getConnections().isEmpty()) {
                server.close();
            }
        }
    }
}
